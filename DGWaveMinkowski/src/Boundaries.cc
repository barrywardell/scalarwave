/*  File produced by Kranc */

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "cctk_Faces.h"
#include "util_Table.h"
#include "Symmetry.h"


/* the boundary treatment is split into 3 steps:    */
/* 1. excision                                      */
/* 2. symmetries                                    */
/* 3. "other" boundary conditions, e.g. radiative */

/* to simplify scheduling and testing, the 3 steps  */
/* are currently applied in separate functions      */


extern "C" void DGWaveMinkowski_CheckBoundaries(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  return;
}

extern "C" void DGWaveMinkowski_SelectBoundConds(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  
  if (CCTK_EQUALS(evolved_scalars_bound, "none"  ) ||
      CCTK_EQUALS(evolved_scalars_bound, "static") ||
      CCTK_EQUALS(evolved_scalars_bound, "flat"  ) ||
      CCTK_EQUALS(evolved_scalars_bound, "zero"  ) )
  {
    ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                      "DGWaveMinkowski::evolved_scalars", evolved_scalars_bound);
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register evolved_scalars_bound BC for DGWaveMinkowski::evolved_scalars!");
  }
  
  if (CCTK_EQUALS(J_group_bound, "none"  ) ||
      CCTK_EQUALS(J_group_bound, "static") ||
      CCTK_EQUALS(J_group_bound, "flat"  ) ||
      CCTK_EQUALS(J_group_bound, "zero"  ) )
  {
    ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                      "DGWaveMinkowski::J_group", J_group_bound);
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register J_group_bound BC for DGWaveMinkowski::J_group!");
  }
  
  if (CCTK_EQUALS(Psi_bound, "none"  ) ||
      CCTK_EQUALS(Psi_bound, "static") ||
      CCTK_EQUALS(Psi_bound, "flat"  ) ||
      CCTK_EQUALS(Psi_bound, "zero"  ) )
  {
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                      "DGWaveMinkowski::Psi", Psi_bound);
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Psi_bound BC for DGWaveMinkowski::Psi!");
  }
  
  if (CCTK_EQUALS(J0_bound, "none"  ) ||
      CCTK_EQUALS(J0_bound, "static") ||
      CCTK_EQUALS(J0_bound, "flat"  ) ||
      CCTK_EQUALS(J0_bound, "zero"  ) )
  {
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                      "DGWaveMinkowski::J0", J0_bound);
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register J0_bound BC for DGWaveMinkowski::J0!");
  }
  
  if (CCTK_EQUALS(J1_bound, "none"  ) ||
      CCTK_EQUALS(J1_bound, "static") ||
      CCTK_EQUALS(J1_bound, "flat"  ) ||
      CCTK_EQUALS(J1_bound, "zero"  ) )
  {
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                      "DGWaveMinkowski::J1", J1_bound);
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register J1_bound BC for DGWaveMinkowski::J1!");
  }
  
  if (CCTK_EQUALS(J2_bound, "none"  ) ||
      CCTK_EQUALS(J2_bound, "static") ||
      CCTK_EQUALS(J2_bound, "flat"  ) ||
      CCTK_EQUALS(J2_bound, "zero"  ) )
  {
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                      "DGWaveMinkowski::J2", J2_bound);
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register J2_bound BC for DGWaveMinkowski::J2!");
  }
  
  if (CCTK_EQUALS(J3_bound, "none"  ) ||
      CCTK_EQUALS(J3_bound, "static") ||
      CCTK_EQUALS(J3_bound, "flat"  ) ||
      CCTK_EQUALS(J3_bound, "zero"  ) )
  {
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                      "DGWaveMinkowski::J3", J3_bound);
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register J3_bound BC for DGWaveMinkowski::J3!");
  }
  
  if (CCTK_EQUALS(evolved_scalars_bound, "radiative"))
  {
   /* select radiation boundary condition */
    static CCTK_INT handle_evolved_scalars_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_evolved_scalars_bound < 0) handle_evolved_scalars_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_evolved_scalars_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_evolved_scalars_bound , evolved_scalars_bound_limit, "LIMIT") < 0)
       CCTK_WARN(0, "could not set LIMIT value in table!");
    if (Util_TableSetReal(handle_evolved_scalars_bound ,evolved_scalars_bound_speed, "SPEED") < 0)
       CCTK_WARN(0, "could not set SPEED value in table!");
  
    ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, handle_evolved_scalars_bound, 
                      "DGWaveMinkowski::evolved_scalars", "Radiation");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Radiation BC for DGWaveMinkowski::evolved_scalars!");
  
  }
  
  if (CCTK_EQUALS(J_group_bound, "radiative"))
  {
   /* select radiation boundary condition */
    static CCTK_INT handle_J_group_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_J_group_bound < 0) handle_J_group_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_J_group_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_J_group_bound , J_group_bound_limit, "LIMIT") < 0)
       CCTK_WARN(0, "could not set LIMIT value in table!");
    if (Util_TableSetReal(handle_J_group_bound ,J_group_bound_speed, "SPEED") < 0)
       CCTK_WARN(0, "could not set SPEED value in table!");
  
    ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, handle_J_group_bound, 
                      "DGWaveMinkowski::J_group", "Radiation");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Radiation BC for DGWaveMinkowski::J_group!");
  
  }
  
  if (CCTK_EQUALS(Psi_bound, "radiative"))
  {
   /* select radiation boundary condition */
    static CCTK_INT handle_Psi_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_Psi_bound < 0) handle_Psi_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_Psi_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_Psi_bound , Psi_bound_limit, "LIMIT") < 0)
       CCTK_WARN(0, "could not set LIMIT value in table!");
    if (Util_TableSetReal(handle_Psi_bound ,Psi_bound_speed, "SPEED") < 0)
        CCTK_WARN(0, "could not set SPEED value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_Psi_bound, 
                      "DGWaveMinkowski::Psi", "Radiation");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Radiation BC for DGWaveMinkowski::Psi!");
  
  }
  
  if (CCTK_EQUALS(J0_bound, "radiative"))
  {
   /* select radiation boundary condition */
    static CCTK_INT handle_J0_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_J0_bound < 0) handle_J0_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_J0_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_J0_bound , J0_bound_limit, "LIMIT") < 0)
       CCTK_WARN(0, "could not set LIMIT value in table!");
    if (Util_TableSetReal(handle_J0_bound ,J0_bound_speed, "SPEED") < 0)
        CCTK_WARN(0, "could not set SPEED value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_J0_bound, 
                      "DGWaveMinkowski::J0", "Radiation");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Radiation BC for DGWaveMinkowski::J0!");
  
  }
  
  if (CCTK_EQUALS(J1_bound, "radiative"))
  {
   /* select radiation boundary condition */
    static CCTK_INT handle_J1_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_J1_bound < 0) handle_J1_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_J1_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_J1_bound , J1_bound_limit, "LIMIT") < 0)
       CCTK_WARN(0, "could not set LIMIT value in table!");
    if (Util_TableSetReal(handle_J1_bound ,J1_bound_speed, "SPEED") < 0)
        CCTK_WARN(0, "could not set SPEED value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_J1_bound, 
                      "DGWaveMinkowski::J1", "Radiation");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Radiation BC for DGWaveMinkowski::J1!");
  
  }
  
  if (CCTK_EQUALS(J2_bound, "radiative"))
  {
   /* select radiation boundary condition */
    static CCTK_INT handle_J2_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_J2_bound < 0) handle_J2_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_J2_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_J2_bound , J2_bound_limit, "LIMIT") < 0)
       CCTK_WARN(0, "could not set LIMIT value in table!");
    if (Util_TableSetReal(handle_J2_bound ,J2_bound_speed, "SPEED") < 0)
        CCTK_WARN(0, "could not set SPEED value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_J2_bound, 
                      "DGWaveMinkowski::J2", "Radiation");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Radiation BC for DGWaveMinkowski::J2!");
  
  }
  
  if (CCTK_EQUALS(J3_bound, "radiative"))
  {
   /* select radiation boundary condition */
    static CCTK_INT handle_J3_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_J3_bound < 0) handle_J3_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_J3_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_J3_bound , J3_bound_limit, "LIMIT") < 0)
       CCTK_WARN(0, "could not set LIMIT value in table!");
    if (Util_TableSetReal(handle_J3_bound ,J3_bound_speed, "SPEED") < 0)
        CCTK_WARN(0, "could not set SPEED value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_J3_bound, 
                      "DGWaveMinkowski::J3", "Radiation");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Radiation BC for DGWaveMinkowski::J3!");
  
  }
  
  if (CCTK_EQUALS(evolved_scalars_bound, "scalar"))
  {
   /* select scalar boundary condition */
    static CCTK_INT handle_evolved_scalars_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_evolved_scalars_bound < 0) handle_evolved_scalars_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_evolved_scalars_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_evolved_scalars_bound ,evolved_scalars_bound_scalar, "SCALAR") < 0)
        CCTK_WARN(0, "could not set SCALAR value in table!");
  
    ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, handle_evolved_scalars_bound, 
                      "DGWaveMinkowski::evolved_scalars", "scalar");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Scalar BC for DGWaveMinkowski::evolved_scalars!");
  
  }
  
  if (CCTK_EQUALS(J_group_bound, "scalar"))
  {
   /* select scalar boundary condition */
    static CCTK_INT handle_J_group_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_J_group_bound < 0) handle_J_group_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_J_group_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_J_group_bound ,J_group_bound_scalar, "SCALAR") < 0)
        CCTK_WARN(0, "could not set SCALAR value in table!");
  
    ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, handle_J_group_bound, 
                      "DGWaveMinkowski::J_group", "scalar");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Scalar BC for DGWaveMinkowski::J_group!");
  
  }
  
  if (CCTK_EQUALS(Psi_bound, "scalar"))
  {
   /* select scalar boundary condition */
    static CCTK_INT handle_Psi_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_Psi_bound < 0) handle_Psi_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_Psi_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_Psi_bound ,Psi_bound_scalar, "SCALAR") < 0)
      CCTK_WARN(0, "could not set SCALAR value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_Psi_bound, 
                      "DGWaveMinkowski::Psi", "scalar");
  
    if (ierr < 0)
       CCTK_WARN(0, "Error in registering Scalar BC for DGWaveMinkowski::Psi!");
  
  }
  
  if (CCTK_EQUALS(J0_bound, "scalar"))
  {
   /* select scalar boundary condition */
    static CCTK_INT handle_J0_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_J0_bound < 0) handle_J0_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_J0_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_J0_bound ,J0_bound_scalar, "SCALAR") < 0)
      CCTK_WARN(0, "could not set SCALAR value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_J0_bound, 
                      "DGWaveMinkowski::J0", "scalar");
  
    if (ierr < 0)
       CCTK_WARN(0, "Error in registering Scalar BC for DGWaveMinkowski::J0!");
  
  }
  
  if (CCTK_EQUALS(J1_bound, "scalar"))
  {
   /* select scalar boundary condition */
    static CCTK_INT handle_J1_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_J1_bound < 0) handle_J1_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_J1_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_J1_bound ,J1_bound_scalar, "SCALAR") < 0)
      CCTK_WARN(0, "could not set SCALAR value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_J1_bound, 
                      "DGWaveMinkowski::J1", "scalar");
  
    if (ierr < 0)
       CCTK_WARN(0, "Error in registering Scalar BC for DGWaveMinkowski::J1!");
  
  }
  
  if (CCTK_EQUALS(J2_bound, "scalar"))
  {
   /* select scalar boundary condition */
    static CCTK_INT handle_J2_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_J2_bound < 0) handle_J2_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_J2_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_J2_bound ,J2_bound_scalar, "SCALAR") < 0)
      CCTK_WARN(0, "could not set SCALAR value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_J2_bound, 
                      "DGWaveMinkowski::J2", "scalar");
  
    if (ierr < 0)
       CCTK_WARN(0, "Error in registering Scalar BC for DGWaveMinkowski::J2!");
  
  }
  
  if (CCTK_EQUALS(J3_bound, "scalar"))
  {
   /* select scalar boundary condition */
    static CCTK_INT handle_J3_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_J3_bound < 0) handle_J3_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_J3_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_J3_bound ,J3_bound_scalar, "SCALAR") < 0)
      CCTK_WARN(0, "could not set SCALAR value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_J3_bound, 
                      "DGWaveMinkowski::J3", "scalar");
  
    if (ierr < 0)
       CCTK_WARN(0, "Error in registering Scalar BC for DGWaveMinkowski::J3!");
  
  }
  return;
}



/* template for entries in parameter file:
#$bound$#DGWaveMinkowski::evolved_scalars_bound       = "skip"
#$bound$#DGWaveMinkowski::evolved_scalars_bound_speed = 1.0
#$bound$#DGWaveMinkowski::evolved_scalars_bound_limit = 0.0
#$bound$#DGWaveMinkowski::evolved_scalars_bound_scalar = 0.0

#$bound$#DGWaveMinkowski::J_group_bound       = "skip"
#$bound$#DGWaveMinkowski::J_group_bound_speed = 1.0
#$bound$#DGWaveMinkowski::J_group_bound_limit = 0.0
#$bound$#DGWaveMinkowski::J_group_bound_scalar = 0.0

#$bound$#DGWaveMinkowski::Psi_bound       = "skip"
#$bound$#DGWaveMinkowski::Psi_bound_speed = 1.0
#$bound$#DGWaveMinkowski::Psi_bound_limit = 0.0
#$bound$#DGWaveMinkowski::Psi_bound_scalar = 0.0

#$bound$#DGWaveMinkowski::J0_bound       = "skip"
#$bound$#DGWaveMinkowski::J0_bound_speed = 1.0
#$bound$#DGWaveMinkowski::J0_bound_limit = 0.0
#$bound$#DGWaveMinkowski::J0_bound_scalar = 0.0

#$bound$#DGWaveMinkowski::J1_bound       = "skip"
#$bound$#DGWaveMinkowski::J1_bound_speed = 1.0
#$bound$#DGWaveMinkowski::J1_bound_limit = 0.0
#$bound$#DGWaveMinkowski::J1_bound_scalar = 0.0

#$bound$#DGWaveMinkowski::J2_bound       = "skip"
#$bound$#DGWaveMinkowski::J2_bound_speed = 1.0
#$bound$#DGWaveMinkowski::J2_bound_limit = 0.0
#$bound$#DGWaveMinkowski::J2_bound_scalar = 0.0

#$bound$#DGWaveMinkowski::J3_bound       = "skip"
#$bound$#DGWaveMinkowski::J3_bound_speed = 1.0
#$bound$#DGWaveMinkowski::J3_bound_limit = 0.0
#$bound$#DGWaveMinkowski::J3_bound_scalar = 0.0

*/

