/*  File produced by Kranc */

#include "cctk.h"

extern "C" int DGWaveMinkowski_Startup(void)
{
  const char* banner CCTK_ATTRIBUTE_UNUSED = "DGWaveMinkowski";
  CCTK_RegisterBanner(banner);
  return 0;
}
