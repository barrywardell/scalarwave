/*  File produced by Kranc */

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

extern "C" void DGWaveMinkowski_RegisterVars(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  /* Register all the evolved grid functions with MoL */
  ierr += MoLRegisterEvolved(CCTK_VarIndex("DGWaveMinkowski::Psi"),  CCTK_VarIndex("DGWaveMinkowski::Psirhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("DGWaveMinkowski::J0"),  CCTK_VarIndex("DGWaveMinkowski::J0rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("DGWaveMinkowski::J1"),  CCTK_VarIndex("DGWaveMinkowski::J1rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("DGWaveMinkowski::J2"),  CCTK_VarIndex("DGWaveMinkowski::J2rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("DGWaveMinkowski::J3"),  CCTK_VarIndex("DGWaveMinkowski::J3rhs"));
  /* Register all the evolved Array functions with MoL */
  return;
}
