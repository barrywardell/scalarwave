/*  File produced by Kranc */

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "cctk_Faces.h"
#include "util_Table.h"
#include "Symmetry.h"


/* the boundary treatment is split into 3 steps:    */
/* 1. excision                                      */
/* 2. symmetries                                    */
/* 3. "other" boundary conditions, e.g. radiative */

/* to simplify scheduling and testing, the 3 steps  */
/* are currently applied in separate functions      */


extern "C" void LDGWaveADMBase_CheckBoundaries(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  return;
}

extern "C" void LDGWaveADMBase_SelectBoundConds(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  CCTK_INT ierr = 0;
  
  if (CCTK_EQUALS(evolved_scalars_bound, "none"  ) ||
      CCTK_EQUALS(evolved_scalars_bound, "static") ||
      CCTK_EQUALS(evolved_scalars_bound, "flat"  ) ||
      CCTK_EQUALS(evolved_scalars_bound, "zero"  ) )
  {
    ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                      "LDGWaveADMBase::evolved_scalars", evolved_scalars_bound);
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register evolved_scalars_bound BC for LDGWaveADMBase::evolved_scalars!");
  }
  
  if (CCTK_EQUALS(J_group_bound, "none"  ) ||
      CCTK_EQUALS(J_group_bound, "static") ||
      CCTK_EQUALS(J_group_bound, "flat"  ) ||
      CCTK_EQUALS(J_group_bound, "zero"  ) )
  {
    ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                      "LDGWaveADMBase::J_group", J_group_bound);
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register J_group_bound BC for LDGWaveADMBase::J_group!");
  }
  
  if (CCTK_EQUALS(Psi_bound, "none"  ) ||
      CCTK_EQUALS(Psi_bound, "static") ||
      CCTK_EQUALS(Psi_bound, "flat"  ) ||
      CCTK_EQUALS(Psi_bound, "zero"  ) )
  {
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                      "LDGWaveADMBase::Psi", Psi_bound);
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Psi_bound BC for LDGWaveADMBase::Psi!");
  }
  
  if (CCTK_EQUALS(dJ0_bound, "none"  ) ||
      CCTK_EQUALS(dJ0_bound, "static") ||
      CCTK_EQUALS(dJ0_bound, "flat"  ) ||
      CCTK_EQUALS(dJ0_bound, "zero"  ) )
  {
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                      "LDGWaveADMBase::dJ0", dJ0_bound);
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register dJ0_bound BC for LDGWaveADMBase::dJ0!");
  }
  
  if (CCTK_EQUALS(J1_bound, "none"  ) ||
      CCTK_EQUALS(J1_bound, "static") ||
      CCTK_EQUALS(J1_bound, "flat"  ) ||
      CCTK_EQUALS(J1_bound, "zero"  ) )
  {
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                      "LDGWaveADMBase::J1", J1_bound);
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register J1_bound BC for LDGWaveADMBase::J1!");
  }
  
  if (CCTK_EQUALS(J2_bound, "none"  ) ||
      CCTK_EQUALS(J2_bound, "static") ||
      CCTK_EQUALS(J2_bound, "flat"  ) ||
      CCTK_EQUALS(J2_bound, "zero"  ) )
  {
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                      "LDGWaveADMBase::J2", J2_bound);
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register J2_bound BC for LDGWaveADMBase::J2!");
  }
  
  if (CCTK_EQUALS(J3_bound, "none"  ) ||
      CCTK_EQUALS(J3_bound, "static") ||
      CCTK_EQUALS(J3_bound, "flat"  ) ||
      CCTK_EQUALS(J3_bound, "zero"  ) )
  {
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                      "LDGWaveADMBase::J3", J3_bound);
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register J3_bound BC for LDGWaveADMBase::J3!");
  }
  
  if (CCTK_EQUALS(evolved_scalars_bound, "radiative"))
  {
   /* select radiation boundary condition */
    static CCTK_INT handle_evolved_scalars_bound = -1;
    if (handle_evolved_scalars_bound < 0) handle_evolved_scalars_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_evolved_scalars_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_evolved_scalars_bound , evolved_scalars_bound_limit, "LIMIT") < 0)
       CCTK_WARN(0, "could not set LIMIT value in table!");
    if (Util_TableSetReal(handle_evolved_scalars_bound ,evolved_scalars_bound_speed, "SPEED") < 0)
       CCTK_WARN(0, "could not set SPEED value in table!");
  
    ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, handle_evolved_scalars_bound, 
                      "LDGWaveADMBase::evolved_scalars", "Radiation");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Radiation BC for LDGWaveADMBase::evolved_scalars!");
  
  }
  
  if (CCTK_EQUALS(J_group_bound, "radiative"))
  {
   /* select radiation boundary condition */
    static CCTK_INT handle_J_group_bound = -1;
    if (handle_J_group_bound < 0) handle_J_group_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_J_group_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_J_group_bound , J_group_bound_limit, "LIMIT") < 0)
       CCTK_WARN(0, "could not set LIMIT value in table!");
    if (Util_TableSetReal(handle_J_group_bound ,J_group_bound_speed, "SPEED") < 0)
       CCTK_WARN(0, "could not set SPEED value in table!");
  
    ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, handle_J_group_bound, 
                      "LDGWaveADMBase::J_group", "Radiation");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Radiation BC for LDGWaveADMBase::J_group!");
  
  }
  
  if (CCTK_EQUALS(Psi_bound, "radiative"))
  {
   /* select radiation boundary condition */
    static CCTK_INT handle_Psi_bound = -1;
    if (handle_Psi_bound < 0) handle_Psi_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_Psi_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_Psi_bound , Psi_bound_limit, "LIMIT") < 0)
       CCTK_WARN(0, "could not set LIMIT value in table!");
    if (Util_TableSetReal(handle_Psi_bound ,Psi_bound_speed, "SPEED") < 0)
        CCTK_WARN(0, "could not set SPEED value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_Psi_bound, 
                      "LDGWaveADMBase::Psi", "Radiation");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Radiation BC for LDGWaveADMBase::Psi!");
  
  }
  
  if (CCTK_EQUALS(dJ0_bound, "radiative"))
  {
   /* select radiation boundary condition */
    static CCTK_INT handle_dJ0_bound = -1;
    if (handle_dJ0_bound < 0) handle_dJ0_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_dJ0_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_dJ0_bound , dJ0_bound_limit, "LIMIT") < 0)
       CCTK_WARN(0, "could not set LIMIT value in table!");
    if (Util_TableSetReal(handle_dJ0_bound ,dJ0_bound_speed, "SPEED") < 0)
        CCTK_WARN(0, "could not set SPEED value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_dJ0_bound, 
                      "LDGWaveADMBase::dJ0", "Radiation");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Radiation BC for LDGWaveADMBase::dJ0!");
  
  }
  
  if (CCTK_EQUALS(J1_bound, "radiative"))
  {
   /* select radiation boundary condition */
    static CCTK_INT handle_J1_bound = -1;
    if (handle_J1_bound < 0) handle_J1_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_J1_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_J1_bound , J1_bound_limit, "LIMIT") < 0)
       CCTK_WARN(0, "could not set LIMIT value in table!");
    if (Util_TableSetReal(handle_J1_bound ,J1_bound_speed, "SPEED") < 0)
        CCTK_WARN(0, "could not set SPEED value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_J1_bound, 
                      "LDGWaveADMBase::J1", "Radiation");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Radiation BC for LDGWaveADMBase::J1!");
  
  }
  
  if (CCTK_EQUALS(J2_bound, "radiative"))
  {
   /* select radiation boundary condition */
    static CCTK_INT handle_J2_bound = -1;
    if (handle_J2_bound < 0) handle_J2_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_J2_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_J2_bound , J2_bound_limit, "LIMIT") < 0)
       CCTK_WARN(0, "could not set LIMIT value in table!");
    if (Util_TableSetReal(handle_J2_bound ,J2_bound_speed, "SPEED") < 0)
        CCTK_WARN(0, "could not set SPEED value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_J2_bound, 
                      "LDGWaveADMBase::J2", "Radiation");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Radiation BC for LDGWaveADMBase::J2!");
  
  }
  
  if (CCTK_EQUALS(J3_bound, "radiative"))
  {
   /* select radiation boundary condition */
    static CCTK_INT handle_J3_bound = -1;
    if (handle_J3_bound < 0) handle_J3_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_J3_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_J3_bound , J3_bound_limit, "LIMIT") < 0)
       CCTK_WARN(0, "could not set LIMIT value in table!");
    if (Util_TableSetReal(handle_J3_bound ,J3_bound_speed, "SPEED") < 0)
        CCTK_WARN(0, "could not set SPEED value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_J3_bound, 
                      "LDGWaveADMBase::J3", "Radiation");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Radiation BC for LDGWaveADMBase::J3!");
  
  }
  
  if (CCTK_EQUALS(evolved_scalars_bound, "scalar"))
  {
   /* select scalar boundary condition */
    static CCTK_INT handle_evolved_scalars_bound = -1;
    if (handle_evolved_scalars_bound < 0) handle_evolved_scalars_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_evolved_scalars_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_evolved_scalars_bound ,evolved_scalars_bound_scalar, "SCALAR") < 0)
        CCTK_WARN(0, "could not set SCALAR value in table!");
  
    ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, handle_evolved_scalars_bound, 
                      "LDGWaveADMBase::evolved_scalars", "scalar");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Scalar BC for LDGWaveADMBase::evolved_scalars!");
  
  }
  
  if (CCTK_EQUALS(J_group_bound, "scalar"))
  {
   /* select scalar boundary condition */
    static CCTK_INT handle_J_group_bound = -1;
    if (handle_J_group_bound < 0) handle_J_group_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_J_group_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_J_group_bound ,J_group_bound_scalar, "SCALAR") < 0)
        CCTK_WARN(0, "could not set SCALAR value in table!");
  
    ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, handle_J_group_bound, 
                      "LDGWaveADMBase::J_group", "scalar");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Scalar BC for LDGWaveADMBase::J_group!");
  
  }
  
  if (CCTK_EQUALS(Psi_bound, "scalar"))
  {
   /* select scalar boundary condition */
    static CCTK_INT handle_Psi_bound = -1;
    if (handle_Psi_bound < 0) handle_Psi_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_Psi_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_Psi_bound ,Psi_bound_scalar, "SCALAR") < 0)
      CCTK_WARN(0, "could not set SCALAR value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_Psi_bound, 
                      "LDGWaveADMBase::Psi", "scalar");
  
    if (ierr < 0)
       CCTK_WARN(0, "Error in registering Scalar BC for LDGWaveADMBase::Psi!");
  
  }
  
  if (CCTK_EQUALS(dJ0_bound, "scalar"))
  {
   /* select scalar boundary condition */
    static CCTK_INT handle_dJ0_bound = -1;
    if (handle_dJ0_bound < 0) handle_dJ0_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_dJ0_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_dJ0_bound ,dJ0_bound_scalar, "SCALAR") < 0)
      CCTK_WARN(0, "could not set SCALAR value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_dJ0_bound, 
                      "LDGWaveADMBase::dJ0", "scalar");
  
    if (ierr < 0)
       CCTK_WARN(0, "Error in registering Scalar BC for LDGWaveADMBase::dJ0!");
  
  }
  
  if (CCTK_EQUALS(J1_bound, "scalar"))
  {
   /* select scalar boundary condition */
    static CCTK_INT handle_J1_bound = -1;
    if (handle_J1_bound < 0) handle_J1_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_J1_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_J1_bound ,J1_bound_scalar, "SCALAR") < 0)
      CCTK_WARN(0, "could not set SCALAR value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_J1_bound, 
                      "LDGWaveADMBase::J1", "scalar");
  
    if (ierr < 0)
       CCTK_WARN(0, "Error in registering Scalar BC for LDGWaveADMBase::J1!");
  
  }
  
  if (CCTK_EQUALS(J2_bound, "scalar"))
  {
   /* select scalar boundary condition */
    static CCTK_INT handle_J2_bound = -1;
    if (handle_J2_bound < 0) handle_J2_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_J2_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_J2_bound ,J2_bound_scalar, "SCALAR") < 0)
      CCTK_WARN(0, "could not set SCALAR value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_J2_bound, 
                      "LDGWaveADMBase::J2", "scalar");
  
    if (ierr < 0)
       CCTK_WARN(0, "Error in registering Scalar BC for LDGWaveADMBase::J2!");
  
  }
  
  if (CCTK_EQUALS(J3_bound, "scalar"))
  {
   /* select scalar boundary condition */
    static CCTK_INT handle_J3_bound = -1;
    if (handle_J3_bound < 0) handle_J3_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_J3_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_J3_bound ,J3_bound_scalar, "SCALAR") < 0)
      CCTK_WARN(0, "could not set SCALAR value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_J3_bound, 
                      "LDGWaveADMBase::J3", "scalar");
  
    if (ierr < 0)
       CCTK_WARN(0, "Error in registering Scalar BC for LDGWaveADMBase::J3!");
  
  }
  return;
}



/* template for entries in parameter file:
#$bound$#LDGWaveADMBase::evolved_scalars_bound       = "skip"
#$bound$#LDGWaveADMBase::evolved_scalars_bound_speed = 1.0
#$bound$#LDGWaveADMBase::evolved_scalars_bound_limit = 0.0
#$bound$#LDGWaveADMBase::evolved_scalars_bound_scalar = 0.0

#$bound$#LDGWaveADMBase::J_group_bound       = "skip"
#$bound$#LDGWaveADMBase::J_group_bound_speed = 1.0
#$bound$#LDGWaveADMBase::J_group_bound_limit = 0.0
#$bound$#LDGWaveADMBase::J_group_bound_scalar = 0.0

#$bound$#LDGWaveADMBase::Psi_bound       = "skip"
#$bound$#LDGWaveADMBase::Psi_bound_speed = 1.0
#$bound$#LDGWaveADMBase::Psi_bound_limit = 0.0
#$bound$#LDGWaveADMBase::Psi_bound_scalar = 0.0

#$bound$#LDGWaveADMBase::dJ0_bound       = "skip"
#$bound$#LDGWaveADMBase::dJ0_bound_speed = 1.0
#$bound$#LDGWaveADMBase::dJ0_bound_limit = 0.0
#$bound$#LDGWaveADMBase::dJ0_bound_scalar = 0.0

#$bound$#LDGWaveADMBase::J1_bound       = "skip"
#$bound$#LDGWaveADMBase::J1_bound_speed = 1.0
#$bound$#LDGWaveADMBase::J1_bound_limit = 0.0
#$bound$#LDGWaveADMBase::J1_bound_scalar = 0.0

#$bound$#LDGWaveADMBase::J2_bound       = "skip"
#$bound$#LDGWaveADMBase::J2_bound_speed = 1.0
#$bound$#LDGWaveADMBase::J2_bound_limit = 0.0
#$bound$#LDGWaveADMBase::J2_bound_scalar = 0.0

#$bound$#LDGWaveADMBase::J3_bound       = "skip"
#$bound$#LDGWaveADMBase::J3_bound_speed = 1.0
#$bound$#LDGWaveADMBase::J3_bound_limit = 0.0
#$bound$#LDGWaveADMBase::J3_bound_scalar = 0.0

*/

