/*  File produced by Kranc */

#define KRANC_C

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "GenericFD.h"
#include "Differencing.h"
#include "cctk_Loop.h"
#include "loopcontrol.h"

/* Define macros used in calculations */
#define INITVALUE (42)
#define QAD(x) (SQR(SQR(x)))
#define INV(x) ((1.0) / (x))
#define SQR(x) ((x) * (x))
#define CUB(x) ((x) * (x) * (x))



/* DGFE Definitions */

#include <hrscc.hh>

#define config_sdg_order      5
#define config_riemann_solver hrscc::LaxFriedrichsRS<DGFE_LDGWaveADMBase_rhs, false>

/* Export definitions */
#define LDGWaveADMBase_rhs_sdg_grid   hrscc::GNIGrid<hrscc::GLLElement<config_sdg_order> >
#define LDGWaveADMBase_rhs_sdg_method hrscc::SDGMethod<DGFE_LDGWaveADMBase_rhs, LDGWaveADMBase_rhs_sdg_grid, config_riemann_solver>

/*** Numerical scheme ***/

/* Configuration */
#define config_method LDGWaveADMBase_rhs_sdg_method

/* Export definitions */
#define LDGWaveADMBase_rhs_method config_method
#define LDGWaveADMBase_rhs_solver hrscc::CLawSolver<DGFE_LDGWaveADMBase_rhs, config_method>



class DGFE_LDGWaveADMBase_rhs;

namespace hrscc {
  template<>
  struct traits<DGFE_LDGWaveADMBase_rhs> {
    // All state vector variables
    enum state_t {idJ0, iJ1, iJ2, iJ3, iPsi, nvars};
    enum {nequations = nvars};
    enum {nexternal = 3*nvars};
    enum {nbitmasks = 0};
    static bool const pure = false;
  };
} // namespace



class DGFE_LDGWaveADMBase_rhs: public hrscc::CLaw<DGFE_LDGWaveADMBase_rhs> {
public:
  typedef hrscc::CLaw<DGFE_LDGWaveADMBase_rhs> claw;
  typedef hrscc::traits<DGFE_LDGWaveADMBase_rhs>::state_t state_t;
  typedef hrscc::traits<DGFE_LDGWaveADMBase_rhs> variables_t;
  static int const nvars = variables_t::nvars;
  
  DGFE_LDGWaveADMBase_rhs();
  
  inline void prim_to_all(hrscc::Observer<claw> & observer) const
  {
  }
  
  template<hrscc::policy::direction_t dir>
  inline void fluxes(hrscc::Observer<claw> & observer) const
  {
    
    CCTK_REAL fluxdJ0L;
    CCTK_REAL fluxJ1L;
    CCTK_REAL fluxJ2L;
    CCTK_REAL fluxJ3L;
    CCTK_REAL fluxPsiL;
    
    switch (dir) {
    case hrscc::policy::x: {
      fluxdJ0L = *observer.field[variables_t::idJ0 + 0*DGFE_LDGWaveADMBase_rhs::nvars];
      fluxJ1L = *observer.field[variables_t::iJ1 + 0*DGFE_LDGWaveADMBase_rhs::nvars];
      fluxJ2L = *observer.field[variables_t::iJ2 + 0*DGFE_LDGWaveADMBase_rhs::nvars];
      fluxJ3L = *observer.field[variables_t::iJ3 + 0*DGFE_LDGWaveADMBase_rhs::nvars];
      fluxPsiL = *observer.field[variables_t::iPsi + 0*DGFE_LDGWaveADMBase_rhs::nvars];
      break;
    }
    case hrscc::policy::y: {
      fluxdJ0L = *observer.field[variables_t::idJ0 + 1*DGFE_LDGWaveADMBase_rhs::nvars];
      fluxJ1L = *observer.field[variables_t::iJ1 + 1*DGFE_LDGWaveADMBase_rhs::nvars];
      fluxJ2L = *observer.field[variables_t::iJ2 + 1*DGFE_LDGWaveADMBase_rhs::nvars];
      fluxJ3L = *observer.field[variables_t::iJ3 + 1*DGFE_LDGWaveADMBase_rhs::nvars];
      fluxPsiL = *observer.field[variables_t::iPsi + 1*DGFE_LDGWaveADMBase_rhs::nvars];
      break;
    }
    case hrscc::policy::z: {
      fluxdJ0L = *observer.field[variables_t::idJ0 + 2*DGFE_LDGWaveADMBase_rhs::nvars];
      fluxJ1L = *observer.field[variables_t::iJ1 + 2*DGFE_LDGWaveADMBase_rhs::nvars];
      fluxJ2L = *observer.field[variables_t::iJ2 + 2*DGFE_LDGWaveADMBase_rhs::nvars];
      fluxJ3L = *observer.field[variables_t::iJ3 + 2*DGFE_LDGWaveADMBase_rhs::nvars];
      fluxPsiL = *observer.field[variables_t::iPsi + 2*DGFE_LDGWaveADMBase_rhs::nvars];
      break;
    }
    default:
      assert(0);
    }
    
    observer.flux[dir][variables_t::idJ0] = fluxdJ0L;
    observer.flux[dir][variables_t::iJ1] = fluxJ1L;
    observer.flux[dir][variables_t::iJ2] = fluxJ2L;
    observer.flux[dir][variables_t::iJ3] = fluxJ3L;
    observer.flux[dir][variables_t::iPsi] = fluxPsiL;
  }
  
  template<hrscc::policy::direction_t dir>
  inline void eigenvalues(hrscc::Observer<claw> & observer) const
  {
    assert(0);
  }
  
  template<hrscc::policy::direction_t dir>
  inline void eig(hrscc::Observer<claw> & observer) const
  {
    assert(0);
  }
};



namespace hrscc {
  template<> int CLaw<DGFE_LDGWaveADMBase_rhs>::conserved_idx[DGFE_LDGWaveADMBase_rhs::nvars] = {};
  template<> int CLaw<DGFE_LDGWaveADMBase_rhs>::primitive_idx[DGFE_LDGWaveADMBase_rhs::nvars] = {};
  template<> int CLaw<DGFE_LDGWaveADMBase_rhs>::rhs_idx[DGFE_LDGWaveADMBase_rhs::nvars] = {};
  template<> int CLaw<DGFE_LDGWaveADMBase_rhs>::field_idx[3*DGFE_LDGWaveADMBase_rhs::nvars] = {};
  template<> int CLaw<DGFE_LDGWaveADMBase_rhs>::bitmask_idx[0] = {};
} // namespace hrscc



namespace {
  int varindex(char const* const varname)
  {
    int const vi = CCTK_VarIndex(varname);
    if (vi<0) CCTK_WARN(CCTK_WARN_ABORT, "Internal error");
    return vi;
  }
}

DGFE_LDGWaveADMBase_rhs::DGFE_LDGWaveADMBase_rhs()
{
  using namespace hrscc;

  CLaw<DGFE_LDGWaveADMBase_rhs>::conserved_idx[variables_t::idJ0] = varindex(CCTK_THORNSTRING "::dJ0");
  CLaw<DGFE_LDGWaveADMBase_rhs>::conserved_idx[variables_t::iJ1] = varindex(CCTK_THORNSTRING "::J1");
  CLaw<DGFE_LDGWaveADMBase_rhs>::conserved_idx[variables_t::iJ2] = varindex(CCTK_THORNSTRING "::J2");
  CLaw<DGFE_LDGWaveADMBase_rhs>::conserved_idx[variables_t::iJ3] = varindex(CCTK_THORNSTRING "::J3");
  CLaw<DGFE_LDGWaveADMBase_rhs>::conserved_idx[variables_t::iPsi] = varindex(CCTK_THORNSTRING "::Psi");
  CLaw<DGFE_LDGWaveADMBase_rhs>::primitive_idx[variables_t::idJ0] = varindex(CCTK_THORNSTRING "::dJ0");
  CLaw<DGFE_LDGWaveADMBase_rhs>::primitive_idx[variables_t::iJ1] = varindex(CCTK_THORNSTRING "::J1");
  CLaw<DGFE_LDGWaveADMBase_rhs>::primitive_idx[variables_t::iJ2] = varindex(CCTK_THORNSTRING "::J2");
  CLaw<DGFE_LDGWaveADMBase_rhs>::primitive_idx[variables_t::iJ3] = varindex(CCTK_THORNSTRING "::J3");
  CLaw<DGFE_LDGWaveADMBase_rhs>::primitive_idx[variables_t::iPsi] = varindex(CCTK_THORNSTRING "::Psi");

  CLaw<DGFE_LDGWaveADMBase_rhs>::field_idx[variables_t::idJ0 + 0*DGFE_LDGWaveADMBase_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxdJ01");
  CLaw<DGFE_LDGWaveADMBase_rhs>::field_idx[variables_t::iJ1 + 0*DGFE_LDGWaveADMBase_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxJ11");
  CLaw<DGFE_LDGWaveADMBase_rhs>::field_idx[variables_t::iJ2 + 0*DGFE_LDGWaveADMBase_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxJ21");
  CLaw<DGFE_LDGWaveADMBase_rhs>::field_idx[variables_t::iJ3 + 0*DGFE_LDGWaveADMBase_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxJ31");
  CLaw<DGFE_LDGWaveADMBase_rhs>::field_idx[variables_t::iPsi + 0*DGFE_LDGWaveADMBase_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxPsi1");
  CLaw<DGFE_LDGWaveADMBase_rhs>::field_idx[variables_t::idJ0 + 1*DGFE_LDGWaveADMBase_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxdJ02");
  CLaw<DGFE_LDGWaveADMBase_rhs>::field_idx[variables_t::iJ1 + 1*DGFE_LDGWaveADMBase_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxJ12");
  CLaw<DGFE_LDGWaveADMBase_rhs>::field_idx[variables_t::iJ2 + 1*DGFE_LDGWaveADMBase_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxJ22");
  CLaw<DGFE_LDGWaveADMBase_rhs>::field_idx[variables_t::iJ3 + 1*DGFE_LDGWaveADMBase_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxJ32");
  CLaw<DGFE_LDGWaveADMBase_rhs>::field_idx[variables_t::iPsi + 1*DGFE_LDGWaveADMBase_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxPsi2");
  CLaw<DGFE_LDGWaveADMBase_rhs>::field_idx[variables_t::idJ0 + 2*DGFE_LDGWaveADMBase_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxdJ03");
  CLaw<DGFE_LDGWaveADMBase_rhs>::field_idx[variables_t::iJ1 + 2*DGFE_LDGWaveADMBase_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxJ13");
  CLaw<DGFE_LDGWaveADMBase_rhs>::field_idx[variables_t::iJ2 + 2*DGFE_LDGWaveADMBase_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxJ23");
  CLaw<DGFE_LDGWaveADMBase_rhs>::field_idx[variables_t::iJ3 + 2*DGFE_LDGWaveADMBase_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxJ33");
  CLaw<DGFE_LDGWaveADMBase_rhs>::field_idx[variables_t::iPsi + 2*DGFE_LDGWaveADMBase_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxPsi3");

  CLaw<DGFE_LDGWaveADMBase_rhs>::rhs_idx[variables_t::idJ0] = varindex(CCTK_THORNSTRING "::dJ0rhs");
  CLaw<DGFE_LDGWaveADMBase_rhs>::rhs_idx[variables_t::iJ1] = varindex(CCTK_THORNSTRING "::J1rhs");
  CLaw<DGFE_LDGWaveADMBase_rhs>::rhs_idx[variables_t::iJ2] = varindex(CCTK_THORNSTRING "::J2rhs");
  CLaw<DGFE_LDGWaveADMBase_rhs>::rhs_idx[variables_t::iJ3] = varindex(CCTK_THORNSTRING "::J3rhs");
  CLaw<DGFE_LDGWaveADMBase_rhs>::rhs_idx[variables_t::iPsi] = varindex(CCTK_THORNSTRING "::Psirhs");
}



/* A solver, DGFE's equivalent of cctkGH */
static LDGWaveADMBase_rhs_solver *solver = NULL;



/* Call the pointwise DGFE derivative operator */
#undef PDstandardNth1
#undef PDstandardNth2
#undef PDstandardNth3
#define PDstandardNth1(u) (solver->diff<hrscc::policy::x>(&(u)[-index], i,j,k))
#define PDstandardNth2(u) (solver->diff<hrscc::policy::y>(&(u)[-index], i,j,k))
#define PDstandardNth3(u) (solver->diff<hrscc::policy::z>(&(u)[-index], i,j,k))




static void LDGWaveADMBase_rhs_Body(cGH const * restrict const cctkGH, int const dir, int const face, CCTK_REAL const normal[3], CCTK_REAL const tangentA[3], CCTK_REAL const tangentB[3], int const imin[3], int const imax[3], int const n_subblock_gfs, CCTK_REAL * restrict const subblock_gfs[])
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  
  /* Include user-supplied include files */
  
  /* Initialise finite differencing variables */
  ptrdiff_t const di = 1;
  ptrdiff_t const dj = CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  ptrdiff_t const dk = CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  ptrdiff_t const cdi = sizeof(CCTK_REAL) * di;
  ptrdiff_t const cdj = sizeof(CCTK_REAL) * dj;
  ptrdiff_t const cdk = sizeof(CCTK_REAL) * dk;
  CCTK_REAL const dx = ToReal(CCTK_DELTA_SPACE(0));
  CCTK_REAL const dy = ToReal(CCTK_DELTA_SPACE(1));
  CCTK_REAL const dz = ToReal(CCTK_DELTA_SPACE(2));
  CCTK_REAL const dt = ToReal(CCTK_DELTA_TIME);
  CCTK_REAL const t = ToReal(cctk_time);
  CCTK_REAL const dxi = INV(dx);
  CCTK_REAL const dyi = INV(dy);
  CCTK_REAL const dzi = INV(dz);
  CCTK_REAL const khalf = 0.5;
  CCTK_REAL const kthird = 1/3.0;
  CCTK_REAL const ktwothird = 2.0/3.0;
  CCTK_REAL const kfourthird = 4.0/3.0;
  CCTK_REAL const keightthird = 8.0/3.0;
  CCTK_REAL const hdxi = 0.5 * dxi;
  CCTK_REAL const hdyi = 0.5 * dyi;
  CCTK_REAL const hdzi = 0.5 * dzi;
  
  /* Initialize predefined quantities */
  CCTK_REAL const p1o2dx = 0.5*INV(dx);
  CCTK_REAL const p1o2dy = 0.5*INV(dy);
  CCTK_REAL const p1o2dz = 0.5*INV(dz);
  CCTK_REAL const p1o4dxdy = 0.25*INV(dx*dy);
  CCTK_REAL const p1o4dxdz = 0.25*INV(dx*dz);
  CCTK_REAL const p1o4dydz = 0.25*INV(dy*dz);
  CCTK_REAL const p1odx2 = INV(SQR(dx));
  CCTK_REAL const p1ody2 = INV(SQR(dy));
  CCTK_REAL const p1odz2 = INV(SQR(dz));
  
  /* Assign local copies of arrays functions */
  
  
  
  /* Calculate temporaries and arrays functions */
  
  /* Copy local copies back to grid functions */
  
  /* Loop over the grid points */
  #pragma omp parallel
  CCTK_LOOP3(LDGWaveADMBase_rhs,
    i,j,k, imin[0],imin[1],imin[2], imax[0],imax[1],imax[2],
    cctk_lsh[0],cctk_lsh[1],cctk_lsh[2])
  {
    ptrdiff_t const index = di*i + dj*j + dk*k;
    
    /* Assign local copies of grid functions */
    
    CCTK_REAL alpL = alp[index];
    CCTK_REAL betaxL = betax[index];
    CCTK_REAL betayL = betay[index];
    CCTK_REAL betazL = betaz[index];
    CCTK_REAL dJ0L = dJ0[index];
    CCTK_REAL gxxL = gxx[index];
    CCTK_REAL gxyL = gxy[index];
    CCTK_REAL gxzL = gxz[index];
    CCTK_REAL gyyL = gyy[index];
    CCTK_REAL gyzL = gyz[index];
    CCTK_REAL gzzL = gzz[index];
    CCTK_REAL J1L = J1[index];
    CCTK_REAL J2L = J2[index];
    CCTK_REAL J3L = J3[index];
    
    
    /* Include user supplied include files */
    
    /* Precompute derivatives */
    
    /* Calculate temporaries and grid functions */
    CCTK_REAL gamma = 2*gxyL*gxzL*gyzL + gzzL*(gxxL*gyyL - 
      SQR(gxyL)) - gyyL*SQR(gxzL) - gxxL*SQR(gyzL);
    
    CCTK_REAL gu11 = INV(-2*gxyL*gxzL*gyzL + 
      gzzL*(-(gxxL*gyyL) + SQR(gxyL)) + gyyL*SQR(gxzL) + 
      gxxL*SQR(gyzL))*(-(gyyL*gzzL) + SQR(gyzL));
    
    CCTK_REAL gu21 = (-(gxzL*gyzL) + 
      gxyL*gzzL)*INV(-2*gxyL*gxzL*gyzL + gzzL*SQR(gxyL) + 
      gyyL*SQR(gxzL) + gxxL*(-(gyyL*gzzL) + SQR(gyzL)));
    
    CCTK_REAL gu31 = (gxzL*gyyL - 
      gxyL*gyzL)*INV(-2*gxyL*gxzL*gyzL + gzzL*(-(gxxL*gyyL) + 
      SQR(gxyL)) + gyyL*SQR(gxzL) + gxxL*SQR(gyzL));
    
    CCTK_REAL gu22 = INV(-2*gxyL*gxzL*gyzL + 
      gzzL*(-(gxxL*gyyL) + SQR(gxyL)) + gyyL*SQR(gxzL) + 
      gxxL*SQR(gyzL))*(-(gxxL*gzzL) + SQR(gxzL));
    
    CCTK_REAL gu32 = (-(gxyL*gxzL) + 
      gxxL*gyzL)*INV(-2*gxyL*gxzL*gyzL + gzzL*SQR(gxyL) + 
      gyyL*SQR(gxzL) + gxxL*(-(gyyL*gzzL) + SQR(gyzL)));
    
    CCTK_REAL gu33 = INV(-2*gxyL*gxzL*gyzL + 
      gzzL*(-(gxxL*gyyL) + SQR(gxyL)) + gyyL*SQR(gxzL) + 
      gxxL*SQR(gyzL))*(-(gxxL*gyyL) + SQR(gxyL));
    
    CCTK_REAL dJd0 = alpL*(-(alpL*dJ0L) + (betaxL*J1L + 
      betayL*J2L + betazL*J3L)*sqrt(gamma));
    
    CCTK_REAL fluxJ11L = -(dJd0*INV(alpL*sqrt(gamma)));
    
    CCTK_REAL fluxJ12L = 0;
    
    CCTK_REAL fluxJ13L = 0;
    
    CCTK_REAL fluxJ21L = 0;
    
    CCTK_REAL fluxJ22L = -(dJd0*INV(alpL*sqrt(gamma)));
    
    CCTK_REAL fluxJ23L = 0;
    
    CCTK_REAL fluxJ31L = 0;
    
    CCTK_REAL fluxJ32L = 0;
    
    CCTK_REAL fluxJ33L = -(dJd0*INV(alpL*sqrt(gamma)));
    
    CCTK_REAL fluxdJ01L = -(betaxL*dJ0L) + alpL*(J1L*gu11 + 
      J2L*gu21 + J3L*gu31)*sqrt(gamma);
    
    CCTK_REAL fluxdJ02L = -(betayL*dJ0L) + alpL*(J1L*gu21 + 
      J2L*gu22 + J3L*gu32)*sqrt(gamma);
    
    CCTK_REAL fluxdJ03L = -(betazL*dJ0L) + alpL*(J1L*gu31 + 
      J2L*gu32 + J3L*gu33)*sqrt(gamma);
    
    CCTK_REAL fluxPsi1L = 0;
    
    CCTK_REAL fluxPsi2L = 0;
    
    CCTK_REAL fluxPsi3L = 0;
    
    CCTK_REAL dJ0rhsL = 0;
    
    CCTK_REAL J1rhsL = 0;
    
    CCTK_REAL J2rhsL = 0;
    
    CCTK_REAL J3rhsL = 0;
    
    CCTK_REAL PsirhsL = dJd0*INV(alpL*sqrt(gamma));
    
    /* Copy local copies back to grid functions */
    dJ0rhs[index] = dJ0rhsL;
    fluxdJ01[index] = fluxdJ01L;
    fluxdJ02[index] = fluxdJ02L;
    fluxdJ03[index] = fluxdJ03L;
    fluxJ11[index] = fluxJ11L;
    fluxJ12[index] = fluxJ12L;
    fluxJ13[index] = fluxJ13L;
    fluxJ21[index] = fluxJ21L;
    fluxJ22[index] = fluxJ22L;
    fluxJ23[index] = fluxJ23L;
    fluxJ31[index] = fluxJ31L;
    fluxJ32[index] = fluxJ32L;
    fluxJ33[index] = fluxJ33L;
    fluxPsi1[index] = fluxPsi1L;
    fluxPsi2[index] = fluxPsi2L;
    fluxPsi3[index] = fluxPsi3L;
    J1rhs[index] = J1rhsL;
    J2rhs[index] = J2rhsL;
    J3rhs[index] = J3rhsL;
    Psirhs[index] = PsirhsL;
  }
  CCTK_ENDLOOP3(LDGWaveADMBase_rhs);
}

extern "C" void LDGWaveADMBase_rhs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering LDGWaveADMBase_rhs_Body");
  }
  
  if (cctk_iteration % LDGWaveADMBase_rhs_calc_every != LDGWaveADMBase_rhs_calc_offset)
  {
    return;
  }
  
  const char *const groups[] = {
    "admbase::lapse",
    "admbase::metric",
    "admbase::shift",
    "LDGWaveADMBase::evolved_scalars",
    "LDGWaveADMBase::evolved_scalarsrhs",
    "LDGWaveADMBase::fluxdJ0_group",
    "LDGWaveADMBase::fluxJ_group",
    "LDGWaveADMBase::fluxPsi_group",
    "LDGWaveADMBase::J_group",
    "LDGWaveADMBase::J_grouprhs"};
  GenericFD_AssertGroupStorage(cctkGH, "LDGWaveADMBase_rhs", 10, groups);
  
  
  
  if (not solver) solver = new LDGWaveADMBase_rhs_method(cctkGH);
  GenericFD_LoopOverEverything(cctkGH, LDGWaveADMBase_rhs_Body);
  
  /* Add the flux terms to the RHS */
  solver->compute_rhs();
  
  delete solver;
  solver = NULL;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving LDGWaveADMBase_rhs_Body");
  }
}
