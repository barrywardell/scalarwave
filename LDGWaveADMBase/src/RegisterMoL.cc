/*  File produced by Kranc */

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

extern "C" void LDGWaveADMBase_RegisterVars(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  CCTK_INT ierr = 0;
  
  /* Register all the evolved grid functions with MoL */
  ierr += MoLRegisterEvolved(CCTK_VarIndex("LDGWaveADMBase::Psi"),  CCTK_VarIndex("LDGWaveADMBase::Psirhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("LDGWaveADMBase::dJ0"),  CCTK_VarIndex("LDGWaveADMBase::dJ0rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("LDGWaveADMBase::J1"),  CCTK_VarIndex("LDGWaveADMBase::J1rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("LDGWaveADMBase::J2"),  CCTK_VarIndex("LDGWaveADMBase::J2rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("LDGWaveADMBase::J3"),  CCTK_VarIndex("LDGWaveADMBase::J3rhs"));
  
  /* Register all the evolved Array functions with MoL */
  return;
}
