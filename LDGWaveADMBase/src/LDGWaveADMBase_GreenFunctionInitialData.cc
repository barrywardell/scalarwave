/*  File produced by Kranc */

#define KRANC_C

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "GenericFD.h"
#include "Differencing.h"
#include "cctk_Loop.h"
#include "loopcontrol.h"

/* Define macros used in calculations */
#define INITVALUE (42)
#define QAD(x) (SQR(SQR(x)))
#define INV(x) ((1.0) / (x))
#define SQR(x) ((x) * (x))
#define CUB(x) ((x) * (x) * (x))



/* DGFE Definitions */

#include <hrscc.hh>

#define config_sdg_order      5
#define config_riemann_solver hrscc::LaxFriedrichsRS<DGFE_LDGWaveADMBase_GreenFunctionInitialData, false>

/* Export definitions */
#define LDGWaveADMBase_GreenFunctionInitialData_sdg_grid   hrscc::GNIGrid<hrscc::GLLElement<config_sdg_order> >
#define LDGWaveADMBase_GreenFunctionInitialData_sdg_method hrscc::SDGMethod<DGFE_LDGWaveADMBase_GreenFunctionInitialData, LDGWaveADMBase_GreenFunctionInitialData_sdg_grid, config_riemann_solver>

/*** Numerical scheme ***/

/* Configuration */
#define config_method LDGWaveADMBase_GreenFunctionInitialData_sdg_method

/* Export definitions */
#define LDGWaveADMBase_GreenFunctionInitialData_method config_method
#define LDGWaveADMBase_GreenFunctionInitialData_solver hrscc::CLawSolver<DGFE_LDGWaveADMBase_GreenFunctionInitialData, config_method>



class DGFE_LDGWaveADMBase_GreenFunctionInitialData;

namespace hrscc {
  template<>
  struct traits<DGFE_LDGWaveADMBase_GreenFunctionInitialData> {
    // All state vector variables
    enum state_t {nvars};
    enum {nequations = nvars};
    enum {nexternal = 3*nvars};
    enum {nbitmasks = 0};
    static bool const pure = false;
  };
} // namespace



class DGFE_LDGWaveADMBase_GreenFunctionInitialData: public hrscc::CLaw<DGFE_LDGWaveADMBase_GreenFunctionInitialData> {
public:
  typedef hrscc::CLaw<DGFE_LDGWaveADMBase_GreenFunctionInitialData> claw;
  typedef hrscc::traits<DGFE_LDGWaveADMBase_GreenFunctionInitialData>::state_t state_t;
  typedef hrscc::traits<DGFE_LDGWaveADMBase_GreenFunctionInitialData> variables_t;
  static int const nvars = variables_t::nvars;
  
  DGFE_LDGWaveADMBase_GreenFunctionInitialData();
  
  inline void prim_to_all(hrscc::Observer<claw> & observer) const
  {
  }
  
  template<hrscc::policy::direction_t dir>
  inline void fluxes(hrscc::Observer<claw> & observer) const
  {
    
    
    switch (dir) {
    case hrscc::policy::x: {
      break;
    }
    case hrscc::policy::y: {
      break;
    }
    case hrscc::policy::z: {
      break;
    }
    default:
      assert(0);
    }
    
  }
  
  template<hrscc::policy::direction_t dir>
  inline void eigenvalues(hrscc::Observer<claw> & observer) const
  {
    assert(0);
  }
  
  template<hrscc::policy::direction_t dir>
  inline void eig(hrscc::Observer<claw> & observer) const
  {
    assert(0);
  }
};



namespace hrscc {
  template<> int CLaw<DGFE_LDGWaveADMBase_GreenFunctionInitialData>::conserved_idx[DGFE_LDGWaveADMBase_GreenFunctionInitialData::nvars] = {};
  template<> int CLaw<DGFE_LDGWaveADMBase_GreenFunctionInitialData>::primitive_idx[DGFE_LDGWaveADMBase_GreenFunctionInitialData::nvars] = {};
  template<> int CLaw<DGFE_LDGWaveADMBase_GreenFunctionInitialData>::rhs_idx[DGFE_LDGWaveADMBase_GreenFunctionInitialData::nvars] = {};
  template<> int CLaw<DGFE_LDGWaveADMBase_GreenFunctionInitialData>::field_idx[3*DGFE_LDGWaveADMBase_GreenFunctionInitialData::nvars] = {};
  template<> int CLaw<DGFE_LDGWaveADMBase_GreenFunctionInitialData>::bitmask_idx[0] = {};
} // namespace hrscc



namespace {
  int varindex(char const* const varname)
  {
    int const vi = CCTK_VarIndex(varname);
    if (vi<0) CCTK_WARN(CCTK_WARN_ABORT, "Internal error");
    return vi;
  }
}

DGFE_LDGWaveADMBase_GreenFunctionInitialData::DGFE_LDGWaveADMBase_GreenFunctionInitialData()
{
  using namespace hrscc;



}



/* A solver, DGFE's equivalent of cctkGH */
static LDGWaveADMBase_GreenFunctionInitialData_solver *solver = NULL;



/* Call the pointwise DGFE derivative operator */
#undef PDstandardNth1
#undef PDstandardNth2
#undef PDstandardNth3
#define PDstandardNth1(u) (solver->diff<hrscc::policy::x>(&(u)[-index], i,j,k))
#define PDstandardNth2(u) (solver->diff<hrscc::policy::y>(&(u)[-index], i,j,k))
#define PDstandardNth3(u) (solver->diff<hrscc::policy::z>(&(u)[-index], i,j,k))




static void LDGWaveADMBase_GreenFunctionInitialData_Body(cGH const * restrict const cctkGH, int const dir, int const face, CCTK_REAL const normal[3], CCTK_REAL const tangentA[3], CCTK_REAL const tangentB[3], int const imin[3], int const imax[3], int const n_subblock_gfs, CCTK_REAL * restrict const subblock_gfs[])
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  
  /* Include user-supplied include files */
  
  /* Initialise finite differencing variables */
  ptrdiff_t const di = 1;
  ptrdiff_t const dj = CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  ptrdiff_t const dk = CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  ptrdiff_t const cdi = sizeof(CCTK_REAL) * di;
  ptrdiff_t const cdj = sizeof(CCTK_REAL) * dj;
  ptrdiff_t const cdk = sizeof(CCTK_REAL) * dk;
  CCTK_REAL const dx = ToReal(CCTK_DELTA_SPACE(0));
  CCTK_REAL const dy = ToReal(CCTK_DELTA_SPACE(1));
  CCTK_REAL const dz = ToReal(CCTK_DELTA_SPACE(2));
  CCTK_REAL const dt = ToReal(CCTK_DELTA_TIME);
  CCTK_REAL const t = ToReal(cctk_time);
  CCTK_REAL const dxi = INV(dx);
  CCTK_REAL const dyi = INV(dy);
  CCTK_REAL const dzi = INV(dz);
  CCTK_REAL const khalf = 0.5;
  CCTK_REAL const kthird = 1/3.0;
  CCTK_REAL const ktwothird = 2.0/3.0;
  CCTK_REAL const kfourthird = 4.0/3.0;
  CCTK_REAL const keightthird = 8.0/3.0;
  CCTK_REAL const hdxi = 0.5 * dxi;
  CCTK_REAL const hdyi = 0.5 * dyi;
  CCTK_REAL const hdzi = 0.5 * dzi;
  
  /* Initialize predefined quantities */
  CCTK_REAL const p1o2dx = 0.5*INV(dx);
  CCTK_REAL const p1o2dy = 0.5*INV(dy);
  CCTK_REAL const p1o2dz = 0.5*INV(dz);
  CCTK_REAL const p1o4dxdy = 0.25*INV(dx*dy);
  CCTK_REAL const p1o4dxdz = 0.25*INV(dx*dz);
  CCTK_REAL const p1o4dydz = 0.25*INV(dy*dz);
  CCTK_REAL const p1odx2 = INV(SQR(dx));
  CCTK_REAL const p1ody2 = INV(SQR(dy));
  CCTK_REAL const p1odz2 = INV(SQR(dz));
  
  /* Assign local copies of arrays functions */
  
  
  
  /* Calculate temporaries and arrays functions */
  
  /* Copy local copies back to grid functions */
  
  /* Loop over the grid points */
  #pragma omp parallel
  CCTK_LOOP3(LDGWaveADMBase_GreenFunctionInitialData,
    i,j,k, imin[0],imin[1],imin[2], imax[0],imax[1],imax[2],
    cctk_lsh[0],cctk_lsh[1],cctk_lsh[2])
  {
    ptrdiff_t const index = di*i + dj*j + dk*k;
    
    /* Assign local copies of grid functions */
    
    CCTK_REAL alpL = alp[index];
    CCTK_REAL gxxL = gxx[index];
    CCTK_REAL gxyL = gxy[index];
    CCTK_REAL gxzL = gxz[index];
    CCTK_REAL gyyL = gyy[index];
    CCTK_REAL gyzL = gyz[index];
    CCTK_REAL gzzL = gzz[index];
    CCTK_REAL xL = x[index];
    CCTK_REAL yL = y[index];
    CCTK_REAL zL = z[index];
    
    
    /* Include user supplied include files */
    
    /* Precompute derivatives */
    
    /* Calculate temporaries and grid functions */
    CCTK_REAL gamma = 2*gxyL*gxzL*gyzL + gzzL*(gxxL*gyyL - 
      SQR(gxyL)) - gyyL*SQR(gxzL) - gxxL*SQR(gyzL);
    
    CCTK_REAL PsiL = 0;
    
    CCTK_REAL dJ0L = -(exp(-0.5*INV(SQR(ToReal(width)))*(SQR(-6 + xL) + 
      SQR(yL) + SQR(zL)))*INV(alpL)*sqrt(gamma)*ToReal(amplitude));
    
    CCTK_REAL J1L = 0;
    
    CCTK_REAL J2L = 0;
    
    CCTK_REAL J3L = 0;
    
    /* Copy local copies back to grid functions */
    dJ0[index] = dJ0L;
    J1[index] = J1L;
    J2[index] = J2L;
    J3[index] = J3L;
    Psi[index] = PsiL;
  }
  CCTK_ENDLOOP3(LDGWaveADMBase_GreenFunctionInitialData);
}

extern "C" void LDGWaveADMBase_GreenFunctionInitialData(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering LDGWaveADMBase_GreenFunctionInitialData_Body");
  }
  
  if (cctk_iteration % LDGWaveADMBase_GreenFunctionInitialData_calc_every != LDGWaveADMBase_GreenFunctionInitialData_calc_offset)
  {
    return;
  }
  
  const char *const groups[] = {
    "admbase::lapse",
    "admbase::metric",
    "LDGWaveADMBase::evolved_scalars",
    "grid::coordinates",
    "LDGWaveADMBase::J_group"};
  GenericFD_AssertGroupStorage(cctkGH, "LDGWaveADMBase_GreenFunctionInitialData", 5, groups);
  
  
  
  if (not solver) solver = new LDGWaveADMBase_GreenFunctionInitialData_method(cctkGH);
  GenericFD_LoopOverEverything(cctkGH, LDGWaveADMBase_GreenFunctionInitialData_Body);
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving LDGWaveADMBase_GreenFunctionInitialData_Body");
  }
}
