/*  File produced by Kranc */

#include "cctk.h"

extern "C" int KrancDGScalarWaveKerrSchild_Startup(void)
{
  const char* banner CCTK_ATTRIBUTE_UNUSED = "KrancDGScalarWaveKerrSchild";
  CCTK_RegisterBanner(banner);
  return 0;
}
