/*  File produced by Kranc */

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

extern "C" void KrancDGScalarWaveKerrSchild_RegisterVars(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  /* Register all the evolved grid functions with MoL */
  ierr += MoLRegisterEvolved(CCTK_VarIndex("KrancDGScalarWaveKerrSchild::phi"),  CCTK_VarIndex("KrancDGScalarWaveKerrSchild::phirhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("KrancDGScalarWaveKerrSchild::rho"),  CCTK_VarIndex("KrancDGScalarWaveKerrSchild::rhorhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("KrancDGScalarWaveKerrSchild::pi1"),  CCTK_VarIndex("KrancDGScalarWaveKerrSchild::pi1rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("KrancDGScalarWaveKerrSchild::pi2"),  CCTK_VarIndex("KrancDGScalarWaveKerrSchild::pi2rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("KrancDGScalarWaveKerrSchild::pi3"),  CCTK_VarIndex("KrancDGScalarWaveKerrSchild::pi3rhs"));
  /* Register all the evolved Array functions with MoL */
  return;
}
