/*  File produced by Kranc */

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

extern "C" void KrancDGScalarWaveMinkowski_RegisterVars(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  /* Register all the evolved grid functions with MoL */
  ierr += MoLRegisterEvolved(CCTK_VarIndex("KrancDGScalarWaveMinkowski::phi"),  CCTK_VarIndex("KrancDGScalarWaveMinkowski::phirhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("KrancDGScalarWaveMinkowski::rho"),  CCTK_VarIndex("KrancDGScalarWaveMinkowski::rhorhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("KrancDGScalarWaveMinkowski::pi1"),  CCTK_VarIndex("KrancDGScalarWaveMinkowski::pi1rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("KrancDGScalarWaveMinkowski::pi2"),  CCTK_VarIndex("KrancDGScalarWaveMinkowski::pi2rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("KrancDGScalarWaveMinkowski::pi3"),  CCTK_VarIndex("KrancDGScalarWaveMinkowski::pi3rhs"));
  /* Register all the evolved Array functions with MoL */
  return;
}
