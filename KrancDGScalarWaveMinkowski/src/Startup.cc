/*  File produced by Kranc */

#include "cctk.h"

extern "C" int KrancDGScalarWaveMinkowski_Startup(void)
{
  const char* banner CCTK_ATTRIBUTE_UNUSED = "KrancDGScalarWaveMinkowski";
  CCTK_RegisterBanner(banner);
  return 0;
}
