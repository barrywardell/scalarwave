(* ::Package:: *)

(* KrancDGScalarWave: A collection of finite difference scalar wave equation thorns
   Copyright 2012 Barry Wardell

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

PrependTo[$Path, "./"];
Get["KrancThorn`"];
Needs["Metrics`"];

SetEnhancedTimes[False];

SetDebugLevel[InfoFull];

(**************************************************************************************)
(* Differencing *)
(**************************************************************************************)

DefineTensor[J];

(*derivs=FD;*)
derivs=DG;
order = 4;

Switch[derivs,
       FD, Get["FDops.m"],
       DG, Get["DGops.m"]];

PD = Deriv;

Dissipation[u_] := Diss[u];

DGParameters = {
  {
    Name        -> alphaDeriv,
    Description -> "Jump term (penalty) strength in derivatives",
    Default     -> 0.5
  },
  {
    Name        -> epsDiss,
    Description -> "Dissipation strength",
    Default     -> 0.0
  },
  {
    Name        -> epsJump,
    Description -> "Penalty strength in dissipation",
    Default     -> 0.0
  },
  {
    Name        -> epsFilter,
    Description -> "Filter strength for first derivatives",
    Default     -> 0.0
  }
};

(**************************************************************************************)
(* Tensors *)
(**************************************************************************************)

Map[DefineTensor, 
{
  beta, gu, dphi, pi, normal
}];

Map[AssertSymmetricDecreasing, 
{
  gu[ua,ub]
}];


(**************************************************************************************)
(* Groups *)
(**************************************************************************************)

(* Cactus group definitions *)
groups = {{"evolved_scalars", {phi, rho}},
          CreateGroupFromTensor[pi[la]],
          CreateGroupFromTensor[dphi[ua]]};

declaredGroupNames = Map[First, groups];


(**************************************************************************************)
(* Shorthands *)
(**************************************************************************************)

shorthands = 
{
  alpha, beta[ua], gamma, gu[ua,ub]
};

(**************************************************************************************)
(* Parameters *)
(**************************************************************************************)

keywordParameters = {
  {
    Name          -> "initial_data",
    AllowedValues -> {"Gaussian", "GreenFunction", "GreenFunctionRadialDerivative", "Sine", "SineX"},
    Default       -> "SineX"
  },
  {
    Name          -> "boundary_condition",
    AllowedValues -> {"none", "6PatchRadiative"},
    Default       -> "none"
  }
};

intParameters =
{
  {
    Name -> fdOrder,
    Default -> 4,
    AllowedValues -> {2,4,6,8}
  }
};

realParameters = {
  {
    Name        -> amplitude,
    Description -> "Amplitude of initial data",
    Default     -> 1.0
  },
  {
    Name        -> width,
    Description -> "Width of initial data",
    Default     -> 1.0
  },
  {
    Name        -> x0,
    Description -> "x coordinate where initial data is centered",
    Default     -> 0.0
  },
  {
    Name        -> y0,
    Description -> "y coordinate where initial data is centered",
    Default     -> 0.0
  },
  {
    Name        -> z0,
    Description -> "z coordinate where initial data is centered",
    Default     -> 0.0
  }
};

(**************************************************************************************)
(* Calculation *)
(**************************************************************************************)

makeKrancFriendly[var_Symbol] := var;
makeKrancFriendly[var_[x___]] := Symbol[ToString[var]<>( StringJoin@@(ToString/@{x}))];
makeKrancFriendly[x_] := x;

scalarWaveThorn[spacetime_, thorn_] :=
  Module[{m, fourMetric, invFourMetric, lapse, shift, threeMetric, detThreeMetric, gij,
          calculations, parameters, coords,
          metricShorthands, shortVars, krancShortVars, extraShorthands},

  Print["Generating thorn for ScalarWave", thorn];

  coords = MetricProperty[spacetime, "Coordinates"];
  If[coords =!= {t, x, y, z},
    Throw["Error, only metrics in Cartesian coordinates are supported"];
  ];

  (* Create new equations for all shorthands and get rules for their derivatives *)
  shorthandEquations = MetricProperty[spacetime, "Shorthands"] /. None -> {};
  shorthandVars = shorthandEquations[[All,1]];

  (* Use simplification hints if provided by the metric to create TransformationFunctions for Simplify *)
  simplifyhints = MetricProperty[spacetime, "SimplifyHints"] /. None -> {};
  tf = {Automatic, Sequence@@(Function[{expr}, expr /. #] & /@
    Flatten[({simplifyhints, shorthandEquations, Reverse /@ shorthandEquations})])};

  (* We pass the transformation functions to Simplify *)
  simpopts = TransformationFunctions -> tf;

  (* If the inverse four metric is not provided then compute it *)
  fourMetric = MetricProperty[spacetime, "Metric"];
  invFourMetric = MetricProperty[spacetime, "InverseMetric"] /. None -> {};
  If[invFourMetric === {},
    invFourMetric = Simplify[Inverse[fourMetric], simpopts];
  ];

  (* Compute lapse, shift, three metric and gij *)
  lapse = 1/Sqrt[-invFourMetric[[1, 1]]];
  shift = Simplify[lapse^2 invFourMetric[[1, 2;;4]], simpopts];
  threeMetric = fourMetric[[2;;4, 2;;4]];
  detThreeMetric = Simplify[Det[threeMetric], simpopts];
  gij = invFourMetric[[2;;4, 2;;4]];

  (* Create Kranc-friencly names for shorthands and corresponding transformation functions *)
  krancShortVars = (# -> makeKrancFriendly[#]) & /@ shorthandVars;
  kranctf = tf /. krancShortVars;

  (* Replace any shorthands with Kranc-friendly versions *)
  lapse 			 = lapse /. krancShortVars;
  shift 		     = shift /. krancShortVars;
  detThreeMetric 	 = detThreeMetric /. krancShortVars;
  gij            	 = gij /. krancShortVars;
  shorthandEquations = shorthandEquations /. krancShortVars;
  shorthandVars 	 = shorthandVars /. krancShortVars;

  (* Get any necessary spacetime parameters *)
  parameters = MetricProperty[spacetime, "Parameters"] /. None -> {};

  calculations =
  {
  {
    Name      -> "KrancDGScalarWave"<>thorn<>"_SetupCoordinates",
    Schedule  -> {"IN KrancDGScalarWave"<>thorn<>"_CalcCoordinatesGroup"},
    Equations -> {
      x -> Node[0,1],
      y -> Node[0,2],
      z -> Node[0,3],
      r -> Sqrt[x^2 + y^2 + z^2]}
  },  
  If[spacetime === "KerrSchild",
  {
    Name -> "KrancDGScalarWave"<>thorn<>"_GreenFunctionInitialData",
    Schedule -> {"AT INITIAL"},
    ConditionalOnKeyword -> {"initial_data", "GreenFunction"},
    Where -> Everywhere,
    Shorthands -> {R},
    Equations ->
    {
      R -> Sqrt[(x^2+y^2+z^2-a^2+Sqrt[(x^2+y^2+z^2-a^2)^2+4 a^2 z^2])/2],
      phi -> 0,
      rho -> 4 Pi / (2 Pi width^2)^(3/2) amplitude Exp[-((x-x0)^2 + (y-y0)^2 + (z-z0)^2)/(2 width^2)] / (1 + 2 M R^3 / (R^4 + a^2 z^2)),
      pi[la] -> 0
    }
  }, Unevaluated[Sequence[]]],
  {
    Name -> "KrancDGScalarWave"<>thorn<>"_GreenFunctionRadialDerivativeInitialData",
    Schedule -> {"AT INITIAL"},
    ConditionalOnKeyword -> {"initial_data", "GreenFunctionRadialDerivative"},
    Where -> Everywhere,
    Equations ->
    {
      phi -> 0,
      rho -> 4 Pi / (2 Pi width^2)^(3/2) 1 / (r^2 * (1 + 2/r)) *
        Exp[-((x - x0)^2 + (y - y0)^2 + (z - z0)^2)/(2 width^2)] *
        (-(x (x - x0) + y (y - y0) + z (z - z0)) r/(width^2) + 2 r),
      pi[la] -> 0
    }
  },
  {
    Name -> "KrancDGScalarWave"<>thorn<>"_Gaussian",
    Schedule -> {"AT INITIAL"},
    ConditionalOnKeyword -> {"initial_data", "Gaussian"},
    Where -> Everywhere,
    Shorthands -> {radius},
    Equations ->
    {
      radius -> Sqrt[x0^2+y0^2+z0^2],
      phi -> amplitude Exp[-(r-radius)^2/(2 width^2)],
      rho -> 0,
      pi1 -> - x/r (r-radius)/width^2 amplitude Exp[-(r-radius)^2/(2 width^2)],
      pi2 -> - y/r (r-radius)/width^2 amplitude Exp[-(r-radius)^2/(2 width^2)],
      pi3 -> - z/r (r-radius)/width^2 amplitude Exp[-(r-radius)^2/(2 width^2)]
    }
  },
  {
    Name                 -> "KrancDGScalarWave"<>thorn<>"_SineInitialData",
    Schedule             -> {"AT initial"},
    ConditionalOnKeyword -> {"initial_data", "Sine"},
    Shorthands           -> {k, omega},
    Equations            -> {
      k     -> Pi / width,
      omega -> Sqrt[3 k^2],
      phi   ->         amplitude Sin[k x + k y + k z - omega t],
      rho   ->   omega amplitude Cos[k x + k y + k z - omega t],
      pi[la]->       k amplitude Cos[k x + k y + k z - omega t]
    },
    NoSimplify -> True           (* otherwise, this takes a very long time *)
  },
  {
    Name                 -> "KrancDGScalarWave"<>thorn<>"_SineXInitialData",
    Schedule             -> {"AT initial"},
    ConditionalOnKeyword -> {"initial_data", "SineX"},
    Shorthands           -> {kx, omega},
    Equations            -> {
      kx    -> Pi / width,
      omega -> Sqrt[kx^2],
      phi   ->         amplitude Sin[kx x - omega t],
      rho   ->   omega amplitude Cos[kx x - omega t],
      pi1   ->      kx amplitude Cos[kx x - omega t],
      pi2   ->       0,
      pi3   ->       0
    },
    NoSimplify -> True           (* otherwise, this takes a very long time *)
  },
  {
    Name -> "KrancDGScalarWave"<>thorn<>"_dphi",
    Schedule -> {"in MoL_CalcRHS"},
    Where -> Everywhere,
    NoSimplify -> True,

    Shorthands -> Join[shorthands, shorthandVars],
    Equations -> Flatten@
    {
      (* Add any shorthand equations *)
      shorthandEquations,

      (* Computed lapse, shift, three metric determinant, g^ij *)
      Table[beta[i] -> shift[[i]], {i, 3}],
      alpha -> lapse,
      gamma -> detThreeMetric,
      Table[gu[i,j]-> gij[[i, j]], {j, 3}, {i, j, 3}],

      (* Term in evolution equations *)
      dphi[ua] -> alpha Sqrt[gamma](gu[ua,ub] pi[lb] + beta[ua] rho / alpha^2)
    }
  },
  {
    Name -> "KrancDGScalarWave"<>thorn<>"_rhs",
    Schedule -> {"in MoL_CalcRHS after KrancDGSScalarWave"<>thorn<>"_dphi"},
    Where -> Interior,
    NoSimplify -> True,

    Shorthands -> Join[shorthands, shorthandVars],
    Equations -> Flatten@
    {
      (* Add any shorthand equations *)
      shorthandEquations,

      (* Computed lapse, shift, three metric determinant, g^ij *)
      Table[beta[i] -> shift[[i]], {i, 3}],
      alpha -> lapse,
      gamma -> detThreeMetric,

      (* Evolution equations *)
      dot[phi] -> rho,
      dot[pi[la]] -> J[ub,la] PD[rho,lb],
      dot[rho] -> beta[ua] J[ub,la] PD[rho,lb] + alpha/Sqrt[gamma] J[ub,la] PD[dphi[ua], lb]
    }
  }
  };

  CreateKrancThornTT[groups, "..", "KrancDGScalarWave"<>thorn,
    Calculations -> calculations,
    DeclaredGroups -> declaredGroupNames,
    Tile -> True,
    MergeFiles -> "KrancDGScalarWave.merge",
    UseJacobian -> True,
    UseLoopControl -> True,
    UseVectors -> False,
    CSE -> False,
    RealParameters -> Join[realParameters, parameters, DGParameters],
    IntParameters -> intParameters,
    KeywordParameters -> keywordParameters
  ];
];

spacetimes = {"KerrSchild", "Minkowski"};
thorns = {"KerrSchild", "Minkowski"};

MapThread[scalarWaveThorn, {spacetimes, thorns}];
