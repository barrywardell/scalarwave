(* ::Package:: *)

(* DGScalarWave: A collection of discontinuous Galerkin scalar wave
   equation thorns
   Copyright 2012 Barry Wardell and David Radice

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

Get["KrancThorn`"];

PrependTo[$Path, "./"];
Needs["Metrics`"];

SetEnhancedTimes[False];


(**************************************************************************************)
(* Differencing *)
(**************************************************************************************)

derivatives =
{
  PDstandardNth[i_]    -> StandardCenteredDifferenceOperator[1,1,i]
};

(**************************************************************************************)
(* Tensors *)
(**************************************************************************************)

Map[DefineTensor, 
{
  beta, gammauu, J, fluxJ, fluxJ0, fluxPsi
}];

Map[AssertSymmetricDecreasing, 
{
  gammauu[ua,ub]
}];


(**************************************************************************************)
(* Groups *)
(**************************************************************************************)

(* Cactus group definitions *)
groups = {{"evolved_scalars", {Psi, J0}},
          CreateGroupFromTensor[fluxPsi[ua]],
          CreateGroupFromTensor[fluxJ0[ua]],
          CreateGroupFromTensor[J[la]],
          CreateGroupFromTensor[fluxJ[la,ub]]};

declaredGroupNames = Map[First, groups];


(**************************************************************************************)
(* Shorthands *)
(**************************************************************************************)

shorthands = 
{
  alpha, beta[ua], gammauu[ua,ub], gamma, dtPsi
};

(**************************************************************************************)
(* Parameters *)
(**************************************************************************************)

keywordParameters = {
  {
    Name          -> "initial_data",
    AllowedValues -> {"GreenFunction", "Sine", "SineX", "standing"},
    Default       -> "SineX"
  },
  {
    Name          -> "boundary_condition",
    AllowedValues -> {"none", "zero", "standing"},
    Default       -> "none"
  }
  };

realParameters = {
  {
    Name        -> amplitude,
    Description -> "Amplitude of initial data",
    Default     -> 1.0
  },
  {
    Name        -> width,
    Description -> "Width of initial data",
    Default     -> 1.0
  }
  };

(**************************************************************************************)
(* Calculation *)
(**************************************************************************************)

makeKrancFriendly[var_Symbol] := var;
makeKrancFriendly[var_[x___]] := Symbol[ToString[var]<>( StringJoin@@(ToString/@{x}))];
makeKrancFriendly[x_] := x;

scalarWaveThorn[spacetime_, thorn_] :=
  Module[{m, fourMetric, invFourMetric, lapse, shift, threeMetric, detThreeMetric, gij,
          calculations, parameters, coords,
          metricShorthands, shortVars, krancShortVars, extraShorthands},

  Print["Generating thorn for DGWave", thorn];

  coords = MetricProperty[spacetime, "Coordinates"];
  If[coords =!= {t, x, y, z},
    Throw["Error, only metrics in Cartesian coordinates are supported"];
  ];

  (* Create new equations for all shorthands and get rules for their derivatives *)
  shorthandEquations = MetricProperty[spacetime, "Shorthands"] /. None -> {};
  shorthandVars = shorthandEquations[[All,1]];

  (* Use simplification hints if provided by the metric to create TransformationFunctions for Simplify *)
  simplifyhints = MetricProperty[spacetime, "SimplifyHints"] /. None -> {};
  tf = {Automatic, Sequence@@(Function[{expr}, expr /. #] & /@
    Flatten[({simplifyhints, shorthandEquations, Reverse /@ shorthandEquations})])};

  (* We pass the transformation functions to Simplify *)
  simpopts = TransformationFunctions -> tf;

  (* If the inverse four metric is not provided then compute it *)
  fourMetric = MetricProperty[spacetime, "Metric"];
  invFourMetric = MetricProperty[spacetime, "InverseMetric"] /. None -> {};
  If[invFourMetric === {},
    invFourMetric = Simplify[Inverse[fourMetric], simpopts];
  ];

  (* Compute lapse, shift, three metric and gij *)
  lapse = 1/Sqrt[-invFourMetric[[1, 1]]];
  shift = Simplify[lapse^2 invFourMetric[[1, 2;;4]], simpopts];
  threeMetric = fourMetric[[2;;4, 2;;4]];
  detThreeMetric = Simplify[Det[threeMetric], simpopts];
  gij = invFourMetric[[2;;4, 2;;4]];

  (* Create Kranc-friencly names for shorthands and corresponding transformation functions *)
  krancShortVars = (# -> makeKrancFriendly[#]) & /@ shorthandVars;
  kranctf = tf /. krancShortVars;

  (* Replace any shorthands with Kranc-friendly versions *)
  lapse 			 = lapse /. krancShortVars;
  shift 		     = shift /. krancShortVars;
  detThreeMetric 	 = detThreeMetric /. krancShortVars;
  gij            	 = gij /. krancShortVars;
  shorthandEquations = shorthandEquations /. krancShortVars;
  shorthandVars 	 = shorthandVars /. krancShortVars;

  (* Get any necessary spacetime parameters *)
  parameters = MetricProperty[spacetime, "Parameters"] /. None -> {};

  initialStanding[t_, x_, y_, z_] :=
    amplitude Cos[k x] Cos[k y] Cos[k z] Cos[omega t];
  initialCalcStanding = {
    Name                 -> "DGWave"<>thorn<>"_Standing",
    Schedule             -> {"AT initial"},
    ConditionalOnKeyword -> {"initial_data", "standing"},
    Shorthands           -> {k, omega},
    Equations            -> {
      k     -> Pi / width,
      omega -> Sqrt[3 k^2],
      Psi   -> initialStanding[t,x,y,z],
      J0    -> - D[initialStanding[t,x,y,z],t],
      J1    -> D[initialStanding[t,x,y,z],x],
      J2    -> D[initialStanding[t,x,y,z],y],
      J3    -> D[initialStanding[t,x,y,z],z]
    },
    NoSimplify -> True
  };

  boundaryCalcStanding = {
    Name                 -> "DGWave"<>thorn<>"_BoundaryStanding",
    Schedule             -> {"IN MoL_PostStep"},
    ConditionalOnKeyword -> {"boundary_condition", "standing"},
    Where                -> Boundary,
    Shorthands           -> {k, omega},
    Equations            -> {
      k     -> Pi / width,
      omega -> Sqrt[3 k^2],
      Psi   -> initialStanding[t,x,y,z],
      J0    -> - D[initialStanding[t,x,y,z],t],
      J1    -> D[initialStanding[t,x,y,z],x],
      J2    -> D[initialStanding[t,x,y,z],y],
      J3    -> D[initialStanding[t,x,y,z],z]
    },
    NoSimplify -> True
  };

  boundaryRHSCalcStanding = {
    Name                 -> "DGWave"<>thorn<>"_BoundaryRHSStanding",
    Schedule             -> {"IN MoL_CalcRHS AFTER DGWave"<>thorn<>"_RHS2",
                             "AT analysis AFTER DGWave"<>thorn<>"_RHS2"},
    ConditionalOnKeyword -> {"boundary_condition", "RHSStanding"},
    Where                -> Boundary,
    Shorthands           -> {k, omega},
    Equations            -> {
      k        -> Pi / width,
      omega    -> Sqrt[3 k^2],
      dot[Psi] -> D[initialStanding[t,x,y,z],t],
      dot[J0]  -> - D[initialStanding[t,x,y,z],t,t],
      dot[J1]  -> D[initialStanding[t,x,y,z],t,x],
      dot[J2]  -> D[initialStanding[t,x,y,z],t,y],
      dot[J3]  -> D[initialStanding[t,x,y,z],t,z]
    },
    NoSimplify -> True
  };

  calculations =
  {
  initialCalcStanding,
  boundaryCalcStanding,
  boundaryRHSCalcStanding,
  {
    Name -> "DGWave"<>thorn<>"_GreenFunctionInitialData",
    Schedule -> {"AT INITIAL"},
    ConditionalOnKeyword -> {"initial_data", "GreenFunction"},
    Where -> Everywhere,
    Shorthands -> Join[shorthands, shorthandVars],
    Equations -> Flatten@
    {
      (* Add any shorthand equations *)
      shorthandEquations,

      alpha -> lapse,
      gamma -> detThreeMetric,

      Psi -> 0,
      J0  -> - amplitude Exp[-((x-6)^2 + (y-0)^2 + (z-0)^2)/(2 width^2)] Sqrt[gamma]/alpha,
      J[la] -> 0
    }
  },
  {
    Name                 -> "DGWave"<>thorn<>"_SineInitialData",
    Schedule             -> {"AT initial"},
    ConditionalOnKeyword -> {"initial_data", "Sine"},
    Shorthands           -> {k, omega},
    Equations            -> {
      k     -> Pi / width,
      omega -> Sqrt[3 k^2],
      Psi   ->         amplitude Sin[k x + k y + k z - omega t],
      J0    ->   omega amplitude Cos[k x + k y + k z - omega t],
      J1    ->       k amplitude Cos[k x + k y + k z - omega t],
      J2    ->       k amplitude Cos[k x + k y + k z - omega t],
      J3    ->       k amplitude Cos[k x + k y + k z - omega t]
    },
    NoSimplify -> True           (* otherwise, this takes a very long time *)
  },
  {
    Name                 -> "DGWave"<>thorn<>"_SineXInitialData",
    Schedule             -> {"AT initial"},
    ConditionalOnKeyword -> {"initial_data", "SineX"},
    Shorthands           -> {kx, omega},
    Equations            -> {
      kx     -> Pi / width,
      omega -> Sqrt[kx^2],
      Psi   ->         amplitude Sin[kx x - omega t],
      J0    ->   omega amplitude Cos[kx x - omega t],
      J1    ->      kx amplitude Cos[kx x - omega t],
      J2    ->       0,
      J3    ->       0
    },
    NoSimplify -> True           (* otherwise, this takes a very long time *)
  },
  {
    Name -> "DGWave"<>thorn<>"_rhs",
    Schedule -> {"in MoL_CalcRHS"},
    Where -> Everywhere,
    UseDGFE -> True,

    Shorthands -> Join[shorthands, shorthandVars],
    Equations -> Flatten@
    {
      (* Add any shorthand equations *)
      shorthandEquations,

      (* Computed lapse, shift, three metric determinant, g^ij *)
      Table[beta[i] -> shift[[i]], {i, 3}],
      alpha -> lapse,
      gamma -> detThreeMetric,
      Table[gammauu[i,j]-> gij[[i, j]] - beta[i] beta[j] / alpha^2, {j, 3}, {i, j, 3}],

      dtPsi -> - alpha J0 / Sqrt[gamma] + beta[ua] J[la],

      (* Fluxes *)
      fluxJ[la,ub] ->  - dtPsi KroneckerDelta[la,ub],
      fluxJ0[ua] -> - J0 beta[ua] + alpha Sqrt[gamma] gammauu[ua,ub] J[lb],
      fluxPsi[ua] -> 0,

      (* Evolution equations *)
      dot[J0] -> 0,
      dot[J[la]] -> 0,
      dot[Psi] -> dtPsi
    },
    NoSimplify -> True
  },
    (******************************************************************************)
    (* Boundary conditions *)
    (******************************************************************************)
  {
      Name      -> "DGWave"<>thorn<>"_BoundaryZero1",
      Schedule  -> {"AT analysis BEFORE (DGWave"<>thorn<>"_rhs1 DGWave"<>thorn<>"_BoundaryRHSZero)"},
      Where -> Everywhere,
      Equations -> {
        dot[Psi]   -> 0,
        dot[J0]    -> 0,
        dot[J[la]] -> 0
      }
  },
  {
      Name      -> "DGWave"<>thorn<>"_BoundaryRHSZero",
      Schedule  -> {"IN MoL_CalcRHS AFTER DGWave"<>thorn<>"_rhs2",
                    "AT analysis AFTER DGWave"<>thorn<>"_rhs2"},
      ConditionalOnKeyword -> {"boundary_condition", "zero"},
      Where -> Boundary,
      Equations -> {
        dot[Psi]   -> 0,
        dot[J0]    -> 0,
        dot[J[la]] -> 0
      }
  },
  {
      Name      -> "DGWave"<>thorn<>"_BoundaryZero",
      Schedule  -> {"IN MoL_PostStep"},
      ConditionalOnKeyword -> {"boundary_condition", "zero"},
      Where -> Boundary,
      Equations -> {
        Psi   -> 0,
        J0    -> 0,
        J[la] -> 0
      }
  }
  };

  CreateKrancThornTT[groups, "..", "DGWave"<>thorn,
    Calculations -> calculations,
    DeclaredGroups -> declaredGroupNames,
    PartialDerivatives -> derivatives,
    UseJacobian -> True,
    UseLoopControl -> True,
    UseVectors -> False,
    CSE -> False,
    UseDGFE -> True,
    RealParameters -> Join[parameters, realParameters],
    KeywordParameters  -> keywordParameters
  ];
];

spacetimes = {"KerrSchild", "Minkowski"};
thorns = {"KerrSchild", "Minkowski"}

MapThread[scalarWaveThorn, {spacetimes, thorns}];
