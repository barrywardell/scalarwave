(* ::Package:: *)

(* DGWaveADMBase: A discontinuous Galerkin scalar wave equation thorn
   Copyright 2012 Barry Wardell and David Radice

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

Get["KrancThorn`"];

SetEnhancedTimes[False];


(**************************************************************************************)
(* Differencing *)
(**************************************************************************************)

derivatives =
{
  PDstandardNth[i_]    -> StandardCenteredDifferenceOperator[1,1,i]
};

(**************************************************************************************)
(* Tensors *)
(**************************************************************************************)

Map[DefineTensor, 
{
  beta, g, gu, J, fluxJ, fluxdJ0, fluxPsi
}];

Map[AssertSymmetricDecreasing, 
{
  gu[ua,ub], g[la,lb]
}];

g11=gxx; g21=gxy; g22=gyy; g31=gxz; g32=gyz; g33=gzz;
beta1=betax; beta2=betay; beta3=betaz;

(**************************************************************************************)
(* Groups *)
(**************************************************************************************)

(* Cactus group definitions *)
evolvedGroups = {{"evolved_scalars", {Psi, dJ0}},
          CreateGroupFromTensor[fluxPsi[ua]],
          CreateGroupFromTensor[fluxdJ0[ua]],
          CreateGroupFromTensor[J[la]],
          CreateGroupFromTensor[fluxJ[la,ub]]};

admGroups = 
  {{"admbase::metric", {gxx,gxy,gxz,gyy,gyz,gzz}},
   {"admbase::lapse", {alp}},
   {"admbase::shift", {betax,betay,betaz}}};

groups = Join[evolvedGroups, admGroups];

declaredGroupNames = Map[First, evolvedGroups];

(**************************************************************************************)
(* Shorthands *)
(**************************************************************************************)

shorthands = 
{
  gu[ua,ub], gamma, dJd0
};

(**************************************************************************************)
(* Parameters *)
(**************************************************************************************)
keywordParameters = {
  {
    Name          -> "initial_data",
    AllowedValues -> {"GreenFunction", "Sine", "SineX"},
    Default       -> "SineX"
  }
  };

realParameters = {
  {
    Name        -> amplitude,
    Description -> "Amplitude of initial data",
    Default     -> 1.0
  },
  {
    Name        -> width,
    Description -> "Width of initial data",
    Default     -> 1.0
  }
  };

(**************************************************************************************)
(* Calculation *)
(**************************************************************************************)

makeKrancFriendly[var_Symbol] := var;
makeKrancFriendly[var_[x___]] := Symbol[ToString[var]<>( StringJoin@@(ToString/@{x}))];
makeKrancFriendly[x_] := x;

calculations =
{
  {
    Name -> "DGWaveADMBase_GreenFunctionInitialData",
    Schedule -> {"AT INITIAL"},
    ConditionalOnKeyword -> {"initial_data", "GreenFunction"},
    Where -> Everywhere,
    Shorthands -> shorthands,
    Equations -> {
      gamma -> Det[MatrixOfComponents[g[la,lb]]],
      Psi -> 0,
      dJ0 -> - amplitude Exp[-((x-6)^2 + (y-0)^2 + (z-0)^2)/(2 width^2)] Sqrt[gamma]/alp,
      J[la] -> 0
    }
  },
  {
    Name                 -> "DGWaveADMBase_SineInitialData",
    Schedule             -> {"AT initial"},
    ConditionalOnKeyword -> {"initial_data", "Sine"},
    Shorthands           -> {k, omega},
    Equations            -> {
      k     -> Pi / width,
      omega -> Sqrt[3 k^2],
      Psi   ->         amplitude Sin[k x + k y + k z - omega t],
      dJ0   ->   omega amplitude Cos[k x + k y + k z - omega t],
      J1    ->       k amplitude Cos[k x + k y + k z - omega t],
      J2    ->       k amplitude Cos[k x + k y + k z - omega t],
      J3    ->       k amplitude Cos[k x + k y + k z - omega t]
    },
    NoSimplify -> True  (* otherwise, this takes a very long time *)
  },
  {
    Name                 -> "DGWaveADMBase_SineXInitialData",
    Schedule             -> {"AT initial"},
    ConditionalOnKeyword -> {"initial_data", "SineX"},
    Shorthands           -> {kx, omega},
    Equations            -> {
      kx     -> Pi / width,
      omega -> Sqrt[kx^2],
      Psi   ->         amplitude Sin[kx x - omega t],
      dJ0   ->   omega amplitude Cos[kx x - omega t],
      J1    ->      kx amplitude Cos[kx x - omega t],
      J2    ->       0,
      J3    ->       0
    },
    NoSimplify -> True           (* otherwise, this takes a very long time *)
  },
  {
    Name -> "DGWaveADMBase_rhs",
    Schedule -> {"in MoL_CalcRHS"},
    Where -> Everywhere,
    UseDGFE -> True,
    
    Shorthands -> shorthands,
    Equations -> Flatten@{
      gamma -> Det[MatrixOfComponents[g[la,lb]]],
      gu[ua,ub] -> MatrixInverse[g[ua,ub]],
   
      dJd0 -> - alp^2 dJ0 + alp Sqrt[gamma] beta[ua] J[la],
    
      (* Fluxes *)
      fluxJ[la,ub] ->  - dJd0 / (alp Sqrt[gamma]) KroneckerDelta[la,ub],
      fluxdJ0[ua] -> - dJ0 beta[ua] + alp Sqrt[gamma] gu[ua,ub] J[lb],
      fluxPsi[ua] -> 0,

      (* Evolution equations *)
      dot[dJ0] -> 0,
      dot[J[la]] -> 0,
      dot[Psi] -> dJd0 / (alp Sqrt[gamma])
    }
  }
};

CreateKrancThornTT[groups, "..", "DGWaveADMBase",
  Calculations -> calculations,
  DeclaredGroups -> declaredGroupNames,
  PartialDerivatives -> derivatives,
  InheritedImplementations -> {"admbase"},
  UseJacobian -> False,
  UseLoopControl -> True,
  UseVectors -> False,
  UseDGFE -> True,
  RealParameters -> realParameters,
  KeywordParameters  -> keywordParameters
];



