(* FD Derivatives *)
(* Copyright 2015 Jonah Miller and Erik Schnetter *)
(* jmiller@perimeterinstitute.ca *)

(* TODO: check that "order" is set and integer and positive and even *)

GFOffsetDir[u_, a_, i_] := ReplacePart[GFOffset[u,0,0,0], {a+1->i}];

diffOpFromStencil[op_, u_] :=
  Simplify[Expand[op GFOffset[u,0,0,0]
                  /. {shift[a_]^i_ :> GFOffsetDir[u,a,i]}
                  /. {shift[a_] :> GFOffsetDir[u,a,1]}]
           //. {GFOffset[u,i_,j_,k_] GFOffset[u,l_,m_,n_] :>
                GFOffset[u,i+l,j+m,k+n]}
           /. {spacing[a_] :> {dx,dy,dz}[[a]]}];

SCDO = StandardCenteredDifferenceOperator;



Node[u_, a_Integer] :=
  ({cctkOriginSpace1,cctkOriginSpace2,cctkOriginSpace3}[[a]] +
   {dx,dy,dz}[[a]] * {cctkLbnd1+i,cctkLbnd2+j,cctkLbnd3+k}[[a]]);

Weight[u_, a_] = 1;
Weight[u_] = Weight[u,1] Weight[u,2] Weight[u,3];

(* use Deriv instead of PD since PD is special in Kranc *)
Deriv[u_, a_Integer] := diffOpFromStencil[SCDO[1, order/2, a], u];
Deriv[u_, a_Integer, a_Integer] := diffOpFromStencil[SCDO[2, order/2, a], u];
Deriv[u_, a_Integer, b_Integer] := diffOpFromStencil[SCDO[1, order/2, a]
                                                     SCDO[1, order/2, b], u];



order2 = Floor[order, 2];

Diss[u_] :=
  ((-1)^(order2/2+1) epsDiss
   diffOpFromStencil[spacing[1]^(order2-1) SCDO[order2, order/2, 1] +
                     spacing[2]^(order2-1) SCDO[order2, order/2, 2] +
                     spacing[3]^(order2-1) SCDO[order2, order/2, 3], u]);
