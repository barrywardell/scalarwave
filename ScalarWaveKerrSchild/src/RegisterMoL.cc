/*  File produced by Kranc */

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

extern "C" void ScalarWaveKerrSchild_RegisterVars(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  /* Register all the evolved grid functions with MoL */
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ScalarWaveKerrSchild::phi"),  CCTK_VarIndex("ScalarWaveKerrSchild::phirhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ScalarWaveKerrSchild::rho"),  CCTK_VarIndex("ScalarWaveKerrSchild::rhorhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ScalarWaveKerrSchild::pi1"),  CCTK_VarIndex("ScalarWaveKerrSchild::pi1rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ScalarWaveKerrSchild::pi2"),  CCTK_VarIndex("ScalarWaveKerrSchild::pi2rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ScalarWaveKerrSchild::pi3"),  CCTK_VarIndex("ScalarWaveKerrSchild::pi3rhs"));
  /* Register all the evolved Array functions with MoL */
  return;
}
