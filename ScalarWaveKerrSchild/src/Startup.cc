/*  File produced by Kranc */

#include "cctk.h"

extern "C" int ScalarWaveKerrSchild_Startup(void)
{
  const char* banner CCTK_ATTRIBUTE_UNUSED = "ScalarWaveKerrSchild";
  CCTK_RegisterBanner(banner);
  return 0;
}
