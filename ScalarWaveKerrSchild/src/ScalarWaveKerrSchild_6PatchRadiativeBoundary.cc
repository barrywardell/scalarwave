/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"
#include "loopcontrol.h"

namespace ScalarWaveKerrSchild {

extern "C" void ScalarWaveKerrSchild_6PatchRadiativeBoundary_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (cctk_iteration % ScalarWaveKerrSchild_6PatchRadiativeBoundary_calc_every != ScalarWaveKerrSchild_6PatchRadiativeBoundary_calc_offset)
    return;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ScalarWaveKerrSchild::evolved_scalarsrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ScalarWaveKerrSchild::evolved_scalarsrhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ScalarWaveKerrSchild::pi_grouprhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ScalarWaveKerrSchild::pi_grouprhs.");
  return;
}

static void ScalarWaveKerrSchild_6PatchRadiativeBoundary_Body(const cGH* restrict const cctkGH, const int dir, const int face, const CCTK_REAL normal[3], const CCTK_REAL tangentA[3], const CCTK_REAL tangentB[3], const int imin[3], const int imax[3], const int n_subblock_gfs, CCTK_REAL* restrict const subblock_gfs[])
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = cctk_time;
  const CCTK_REAL cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_TIME;
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 
    0.333333333333333333333333333333;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 
    0.666666666666666666666666666667;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 
    1.33333333333333333333333333333;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5*dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5*dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5*dzi;
  /* Initialize predefined quantities */
  const CCTK_REAL p1o12dx CCTK_ATTRIBUTE_UNUSED = 0.0833333333333333333333333333333*pow(dx,-1);
  const CCTK_REAL p1o12dy CCTK_ATTRIBUTE_UNUSED = 0.0833333333333333333333333333333*pow(dy,-1);
  const CCTK_REAL p1o12dz CCTK_ATTRIBUTE_UNUSED = 0.0833333333333333333333333333333*pow(dz,-1);
  const CCTK_REAL p1o2dx CCTK_ATTRIBUTE_UNUSED = 0.5*pow(dx,-1);
  const CCTK_REAL p1o2dy CCTK_ATTRIBUTE_UNUSED = 0.5*pow(dy,-1);
  const CCTK_REAL p1o2dz CCTK_ATTRIBUTE_UNUSED = 0.5*pow(dz,-1);
  const CCTK_REAL p1o60dx CCTK_ATTRIBUTE_UNUSED = 0.0166666666666666666666666666667*pow(dx,-1);
  const CCTK_REAL p1o60dy CCTK_ATTRIBUTE_UNUSED = 0.0166666666666666666666666666667*pow(dy,-1);
  const CCTK_REAL p1o60dz CCTK_ATTRIBUTE_UNUSED = 0.0166666666666666666666666666667*pow(dz,-1);
  const CCTK_REAL p1o840dx CCTK_ATTRIBUTE_UNUSED = 0.00119047619047619047619047619048*pow(dx,-1);
  const CCTK_REAL p1o840dy CCTK_ATTRIBUTE_UNUSED = 0.00119047619047619047619047619048*pow(dy,-1);
  const CCTK_REAL p1o840dz CCTK_ATTRIBUTE_UNUSED = 0.00119047619047619047619047619048*pow(dz,-1);
  const CCTK_REAL pm1o2dz CCTK_ATTRIBUTE_UNUSED = -0.5*pow(dz,-1);
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3(ScalarWaveKerrSchild_6PatchRadiativeBoundary,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2])
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    /* Assign local copies of grid functions */
    
    CCTK_REAL phiL CCTK_ATTRIBUTE_UNUSED = phi[index];
    CCTK_REAL pi1L CCTK_ATTRIBUTE_UNUSED = pi1[index];
    CCTK_REAL pi2L CCTK_ATTRIBUTE_UNUSED = pi2[index];
    CCTK_REAL pi3L CCTK_ATTRIBUTE_UNUSED = pi3[index];
    CCTK_REAL rL CCTK_ATTRIBUTE_UNUSED = r[index];
    CCTK_REAL rhoL CCTK_ATTRIBUTE_UNUSED = rho[index];
    
    /* Include user supplied include files */
    /* Precompute derivatives */
    CCTK_REAL PDstandard2nd3phi CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDonesidedPlus2nd3phi CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDonesidedMinus2nd3phi CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDstandard2nd3pi1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDonesidedPlus2nd3pi1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDonesidedMinus2nd3pi1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDstandard2nd3pi2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDonesidedPlus2nd3pi2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDonesidedMinus2nd3pi2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDstandard2nd3pi3 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDonesidedPlus2nd3pi3 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDonesidedMinus2nd3pi3 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDstandard2nd3rho CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDonesidedPlus2nd3rho CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDonesidedMinus2nd3rho CCTK_ATTRIBUTE_UNUSED;
    
    switch (fdOrder)
    {
      case 2:
      {
        PDstandard2nd3phi = PDstandard2nd3(&phi[index]);
        PDonesidedPlus2nd3phi = PDonesidedPlus2nd3(&phi[index]);
        PDonesidedMinus2nd3phi = PDonesidedMinus2nd3(&phi[index]);
        PDstandard2nd3pi1 = PDstandard2nd3(&pi1[index]);
        PDonesidedPlus2nd3pi1 = PDonesidedPlus2nd3(&pi1[index]);
        PDonesidedMinus2nd3pi1 = PDonesidedMinus2nd3(&pi1[index]);
        PDstandard2nd3pi2 = PDstandard2nd3(&pi2[index]);
        PDonesidedPlus2nd3pi2 = PDonesidedPlus2nd3(&pi2[index]);
        PDonesidedMinus2nd3pi2 = PDonesidedMinus2nd3(&pi2[index]);
        PDstandard2nd3pi3 = PDstandard2nd3(&pi3[index]);
        PDonesidedPlus2nd3pi3 = PDonesidedPlus2nd3(&pi3[index]);
        PDonesidedMinus2nd3pi3 = PDonesidedMinus2nd3(&pi3[index]);
        PDstandard2nd3rho = PDstandard2nd3(&rho[index]);
        PDonesidedPlus2nd3rho = PDonesidedPlus2nd3(&rho[index]);
        PDonesidedMinus2nd3rho = PDonesidedMinus2nd3(&rho[index]);
        break;
      }
      
      case 4:
      {
        PDstandard2nd3phi = PDstandard2nd3(&phi[index]);
        PDonesidedPlus2nd3phi = PDonesidedPlus2nd3(&phi[index]);
        PDonesidedMinus2nd3phi = PDonesidedMinus2nd3(&phi[index]);
        PDstandard2nd3pi1 = PDstandard2nd3(&pi1[index]);
        PDonesidedPlus2nd3pi1 = PDonesidedPlus2nd3(&pi1[index]);
        PDonesidedMinus2nd3pi1 = PDonesidedMinus2nd3(&pi1[index]);
        PDstandard2nd3pi2 = PDstandard2nd3(&pi2[index]);
        PDonesidedPlus2nd3pi2 = PDonesidedPlus2nd3(&pi2[index]);
        PDonesidedMinus2nd3pi2 = PDonesidedMinus2nd3(&pi2[index]);
        PDstandard2nd3pi3 = PDstandard2nd3(&pi3[index]);
        PDonesidedPlus2nd3pi3 = PDonesidedPlus2nd3(&pi3[index]);
        PDonesidedMinus2nd3pi3 = PDonesidedMinus2nd3(&pi3[index]);
        PDstandard2nd3rho = PDstandard2nd3(&rho[index]);
        PDonesidedPlus2nd3rho = PDonesidedPlus2nd3(&rho[index]);
        PDonesidedMinus2nd3rho = PDonesidedMinus2nd3(&rho[index]);
        break;
      }
      
      case 6:
      {
        PDstandard2nd3phi = PDstandard2nd3(&phi[index]);
        PDonesidedPlus2nd3phi = PDonesidedPlus2nd3(&phi[index]);
        PDonesidedMinus2nd3phi = PDonesidedMinus2nd3(&phi[index]);
        PDstandard2nd3pi1 = PDstandard2nd3(&pi1[index]);
        PDonesidedPlus2nd3pi1 = PDonesidedPlus2nd3(&pi1[index]);
        PDonesidedMinus2nd3pi1 = PDonesidedMinus2nd3(&pi1[index]);
        PDstandard2nd3pi2 = PDstandard2nd3(&pi2[index]);
        PDonesidedPlus2nd3pi2 = PDonesidedPlus2nd3(&pi2[index]);
        PDonesidedMinus2nd3pi2 = PDonesidedMinus2nd3(&pi2[index]);
        PDstandard2nd3pi3 = PDstandard2nd3(&pi3[index]);
        PDonesidedPlus2nd3pi3 = PDonesidedPlus2nd3(&pi3[index]);
        PDonesidedMinus2nd3pi3 = PDonesidedMinus2nd3(&pi3[index]);
        PDstandard2nd3rho = PDstandard2nd3(&rho[index]);
        PDonesidedPlus2nd3rho = PDonesidedPlus2nd3(&rho[index]);
        PDonesidedMinus2nd3rho = PDonesidedMinus2nd3(&rho[index]);
        break;
      }
      
      case 8:
      {
        PDstandard2nd3phi = PDstandard2nd3(&phi[index]);
        PDonesidedPlus2nd3phi = PDonesidedPlus2nd3(&phi[index]);
        PDonesidedMinus2nd3phi = PDonesidedMinus2nd3(&phi[index]);
        PDstandard2nd3pi1 = PDstandard2nd3(&pi1[index]);
        PDonesidedPlus2nd3pi1 = PDonesidedPlus2nd3(&pi1[index]);
        PDonesidedMinus2nd3pi1 = PDonesidedMinus2nd3(&pi1[index]);
        PDstandard2nd3pi2 = PDstandard2nd3(&pi2[index]);
        PDonesidedPlus2nd3pi2 = PDonesidedPlus2nd3(&pi2[index]);
        PDonesidedMinus2nd3pi2 = PDonesidedMinus2nd3(&pi2[index]);
        PDstandard2nd3pi3 = PDstandard2nd3(&pi3[index]);
        PDonesidedPlus2nd3pi3 = PDonesidedPlus2nd3(&pi3[index]);
        PDonesidedMinus2nd3pi3 = PDonesidedMinus2nd3(&pi3[index]);
        PDstandard2nd3rho = PDstandard2nd3(&rho[index]);
        PDonesidedPlus2nd3rho = PDonesidedPlus2nd3(&rho[index]);
        PDonesidedMinus2nd3rho = PDonesidedMinus2nd3(&rho[index]);
        break;
      }
      default:
        CCTK_BUILTIN_UNREACHABLE();
    }
    /* Calculate temporaries and grid functions */
    CCTK_REAL v0 CCTK_ATTRIBUTE_UNUSED = IfThen(normal[2] < 0,-1,1);
    
    CCTK_REAL phirhsL CCTK_ATTRIBUTE_UNUSED = -(v0*(IfThen(normal[2] < 
      0,PDonesidedPlus2nd3phi,IfThen(normal[2] > 
      0,PDonesidedMinus2nd3phi,PDstandard2nd3phi)) + phiL*pow(rL,-1)));
    
    CCTK_REAL pi1rhsL CCTK_ATTRIBUTE_UNUSED = -(v0*(IfThen(normal[2] < 
      0,PDonesidedPlus2nd3pi1,IfThen(normal[2] > 
      0,PDonesidedMinus2nd3pi1,PDstandard2nd3pi1)) + pi1L*pow(rL,-1)));
    
    CCTK_REAL pi2rhsL CCTK_ATTRIBUTE_UNUSED = -(v0*(IfThen(normal[2] < 
      0,PDonesidedPlus2nd3pi2,IfThen(normal[2] > 
      0,PDonesidedMinus2nd3pi2,PDstandard2nd3pi2)) + pi2L*pow(rL,-1)));
    
    CCTK_REAL pi3rhsL CCTK_ATTRIBUTE_UNUSED = -(v0*(IfThen(normal[2] < 
      0,PDonesidedPlus2nd3pi3,IfThen(normal[2] > 
      0,PDonesidedMinus2nd3pi3,PDstandard2nd3pi3)) + pi3L*pow(rL,-1)));
    
    CCTK_REAL rhorhsL CCTK_ATTRIBUTE_UNUSED = -(v0*(IfThen(normal[2] < 
      0,PDonesidedPlus2nd3rho,IfThen(normal[2] > 
      0,PDonesidedMinus2nd3rho,PDstandard2nd3rho)) + rhoL*pow(rL,-1)));
    /* Copy local copies back to grid functions */
    phirhs[index] = phirhsL;
    pi1rhs[index] = pi1rhsL;
    pi2rhs[index] = pi2rhsL;
    pi3rhs[index] = pi3rhsL;
    rhorhs[index] = rhorhsL;
  }
  CCTK_ENDLOOP3(ScalarWaveKerrSchild_6PatchRadiativeBoundary);
}
extern "C" void ScalarWaveKerrSchild_6PatchRadiativeBoundary(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering ScalarWaveKerrSchild_6PatchRadiativeBoundary_Body");
  }
  if (cctk_iteration % ScalarWaveKerrSchild_6PatchRadiativeBoundary_calc_every != ScalarWaveKerrSchild_6PatchRadiativeBoundary_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "ScalarWaveKerrSchild::evolved_scalars",
    "ScalarWaveKerrSchild::evolved_scalarsrhs",
    "grid::coordinates",
    "ScalarWaveKerrSchild::pi_group",
    "ScalarWaveKerrSchild::pi_grouprhs"};
  AssertGroupStorage(cctkGH, "ScalarWaveKerrSchild_6PatchRadiativeBoundary", 5, groups);
  
  switch (fdOrder)
  {
    case 2:
    {
      EnsureStencilFits(cctkGH, "ScalarWaveKerrSchild_6PatchRadiativeBoundary", 0, 0, 2);
      break;
    }
    
    case 4:
    {
      EnsureStencilFits(cctkGH, "ScalarWaveKerrSchild_6PatchRadiativeBoundary", 0, 0, 2);
      break;
    }
    
    case 6:
    {
      EnsureStencilFits(cctkGH, "ScalarWaveKerrSchild_6PatchRadiativeBoundary", 0, 0, 2);
      break;
    }
    
    case 8:
    {
      EnsureStencilFits(cctkGH, "ScalarWaveKerrSchild_6PatchRadiativeBoundary", 0, 0, 2);
      break;
    }
    default:
      CCTK_BUILTIN_UNREACHABLE();
  }
  
  LoopOverBoundary(cctkGH, ScalarWaveKerrSchild_6PatchRadiativeBoundary_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving ScalarWaveKerrSchild_6PatchRadiativeBoundary_Body");
  }
}

} // namespace ScalarWaveKerrSchild
