/*  File produced by Kranc */

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "cctk_Faces.h"
#include "util_Table.h"
#include "Symmetry.h"


/* the boundary treatment is split into 3 steps:    */
/* 1. excision                                      */
/* 2. symmetries                                    */
/* 3. "other" boundary conditions, e.g. radiative */

/* to simplify scheduling and testing, the 3 steps  */
/* are currently applied in separate functions      */


extern "C" void ScalarWaveKerrSchild_CheckBoundaries(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  return;
}

extern "C" void ScalarWaveKerrSchild_SelectBoundConds(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  
  if (CCTK_EQUALS(evolved_scalars_bound, "none"  ) ||
      CCTK_EQUALS(evolved_scalars_bound, "static") ||
      CCTK_EQUALS(evolved_scalars_bound, "flat"  ) ||
      CCTK_EQUALS(evolved_scalars_bound, "zero"  ) )
  {
    ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                      "ScalarWaveKerrSchild::evolved_scalars", evolved_scalars_bound);
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register evolved_scalars_bound BC for ScalarWaveKerrSchild::evolved_scalars!");
  }
  
  if (CCTK_EQUALS(pi_group_bound, "none"  ) ||
      CCTK_EQUALS(pi_group_bound, "static") ||
      CCTK_EQUALS(pi_group_bound, "flat"  ) ||
      CCTK_EQUALS(pi_group_bound, "zero"  ) )
  {
    ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                      "ScalarWaveKerrSchild::pi_group", pi_group_bound);
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register pi_group_bound BC for ScalarWaveKerrSchild::pi_group!");
  }
  
  if (CCTK_EQUALS(phi_bound, "none"  ) ||
      CCTK_EQUALS(phi_bound, "static") ||
      CCTK_EQUALS(phi_bound, "flat"  ) ||
      CCTK_EQUALS(phi_bound, "zero"  ) )
  {
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                      "ScalarWaveKerrSchild::phi", phi_bound);
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register phi_bound BC for ScalarWaveKerrSchild::phi!");
  }
  
  if (CCTK_EQUALS(rho_bound, "none"  ) ||
      CCTK_EQUALS(rho_bound, "static") ||
      CCTK_EQUALS(rho_bound, "flat"  ) ||
      CCTK_EQUALS(rho_bound, "zero"  ) )
  {
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                      "ScalarWaveKerrSchild::rho", rho_bound);
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register rho_bound BC for ScalarWaveKerrSchild::rho!");
  }
  
  if (CCTK_EQUALS(pi1_bound, "none"  ) ||
      CCTK_EQUALS(pi1_bound, "static") ||
      CCTK_EQUALS(pi1_bound, "flat"  ) ||
      CCTK_EQUALS(pi1_bound, "zero"  ) )
  {
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                      "ScalarWaveKerrSchild::pi1", pi1_bound);
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register pi1_bound BC for ScalarWaveKerrSchild::pi1!");
  }
  
  if (CCTK_EQUALS(pi2_bound, "none"  ) ||
      CCTK_EQUALS(pi2_bound, "static") ||
      CCTK_EQUALS(pi2_bound, "flat"  ) ||
      CCTK_EQUALS(pi2_bound, "zero"  ) )
  {
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                      "ScalarWaveKerrSchild::pi2", pi2_bound);
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register pi2_bound BC for ScalarWaveKerrSchild::pi2!");
  }
  
  if (CCTK_EQUALS(pi3_bound, "none"  ) ||
      CCTK_EQUALS(pi3_bound, "static") ||
      CCTK_EQUALS(pi3_bound, "flat"  ) ||
      CCTK_EQUALS(pi3_bound, "zero"  ) )
  {
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                      "ScalarWaveKerrSchild::pi3", pi3_bound);
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register pi3_bound BC for ScalarWaveKerrSchild::pi3!");
  }
  
  if (CCTK_EQUALS(evolved_scalars_bound, "radiative"))
  {
   /* select radiation boundary condition */
    static CCTK_INT handle_evolved_scalars_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_evolved_scalars_bound < 0) handle_evolved_scalars_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_evolved_scalars_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_evolved_scalars_bound , evolved_scalars_bound_limit, "LIMIT") < 0)
       CCTK_WARN(0, "could not set LIMIT value in table!");
    if (Util_TableSetReal(handle_evolved_scalars_bound ,evolved_scalars_bound_speed, "SPEED") < 0)
       CCTK_WARN(0, "could not set SPEED value in table!");
  
    ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, handle_evolved_scalars_bound, 
                      "ScalarWaveKerrSchild::evolved_scalars", "Radiation");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Radiation BC for ScalarWaveKerrSchild::evolved_scalars!");
  
  }
  
  if (CCTK_EQUALS(pi_group_bound, "radiative"))
  {
   /* select radiation boundary condition */
    static CCTK_INT handle_pi_group_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_pi_group_bound < 0) handle_pi_group_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_pi_group_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_pi_group_bound , pi_group_bound_limit, "LIMIT") < 0)
       CCTK_WARN(0, "could not set LIMIT value in table!");
    if (Util_TableSetReal(handle_pi_group_bound ,pi_group_bound_speed, "SPEED") < 0)
       CCTK_WARN(0, "could not set SPEED value in table!");
  
    ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, handle_pi_group_bound, 
                      "ScalarWaveKerrSchild::pi_group", "Radiation");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Radiation BC for ScalarWaveKerrSchild::pi_group!");
  
  }
  
  if (CCTK_EQUALS(phi_bound, "radiative"))
  {
   /* select radiation boundary condition */
    static CCTK_INT handle_phi_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_phi_bound < 0) handle_phi_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_phi_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_phi_bound , phi_bound_limit, "LIMIT") < 0)
       CCTK_WARN(0, "could not set LIMIT value in table!");
    if (Util_TableSetReal(handle_phi_bound ,phi_bound_speed, "SPEED") < 0)
        CCTK_WARN(0, "could not set SPEED value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_phi_bound, 
                      "ScalarWaveKerrSchild::phi", "Radiation");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Radiation BC for ScalarWaveKerrSchild::phi!");
  
  }
  
  if (CCTK_EQUALS(rho_bound, "radiative"))
  {
   /* select radiation boundary condition */
    static CCTK_INT handle_rho_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_rho_bound < 0) handle_rho_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_rho_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_rho_bound , rho_bound_limit, "LIMIT") < 0)
       CCTK_WARN(0, "could not set LIMIT value in table!");
    if (Util_TableSetReal(handle_rho_bound ,rho_bound_speed, "SPEED") < 0)
        CCTK_WARN(0, "could not set SPEED value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_rho_bound, 
                      "ScalarWaveKerrSchild::rho", "Radiation");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Radiation BC for ScalarWaveKerrSchild::rho!");
  
  }
  
  if (CCTK_EQUALS(pi1_bound, "radiative"))
  {
   /* select radiation boundary condition */
    static CCTK_INT handle_pi1_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_pi1_bound < 0) handle_pi1_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_pi1_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_pi1_bound , pi1_bound_limit, "LIMIT") < 0)
       CCTK_WARN(0, "could not set LIMIT value in table!");
    if (Util_TableSetReal(handle_pi1_bound ,pi1_bound_speed, "SPEED") < 0)
        CCTK_WARN(0, "could not set SPEED value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_pi1_bound, 
                      "ScalarWaveKerrSchild::pi1", "Radiation");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Radiation BC for ScalarWaveKerrSchild::pi1!");
  
  }
  
  if (CCTK_EQUALS(pi2_bound, "radiative"))
  {
   /* select radiation boundary condition */
    static CCTK_INT handle_pi2_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_pi2_bound < 0) handle_pi2_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_pi2_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_pi2_bound , pi2_bound_limit, "LIMIT") < 0)
       CCTK_WARN(0, "could not set LIMIT value in table!");
    if (Util_TableSetReal(handle_pi2_bound ,pi2_bound_speed, "SPEED") < 0)
        CCTK_WARN(0, "could not set SPEED value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_pi2_bound, 
                      "ScalarWaveKerrSchild::pi2", "Radiation");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Radiation BC for ScalarWaveKerrSchild::pi2!");
  
  }
  
  if (CCTK_EQUALS(pi3_bound, "radiative"))
  {
   /* select radiation boundary condition */
    static CCTK_INT handle_pi3_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_pi3_bound < 0) handle_pi3_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_pi3_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_pi3_bound , pi3_bound_limit, "LIMIT") < 0)
       CCTK_WARN(0, "could not set LIMIT value in table!");
    if (Util_TableSetReal(handle_pi3_bound ,pi3_bound_speed, "SPEED") < 0)
        CCTK_WARN(0, "could not set SPEED value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_pi3_bound, 
                      "ScalarWaveKerrSchild::pi3", "Radiation");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Radiation BC for ScalarWaveKerrSchild::pi3!");
  
  }
  
  if (CCTK_EQUALS(evolved_scalars_bound, "scalar"))
  {
   /* select scalar boundary condition */
    static CCTK_INT handle_evolved_scalars_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_evolved_scalars_bound < 0) handle_evolved_scalars_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_evolved_scalars_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_evolved_scalars_bound ,evolved_scalars_bound_scalar, "SCALAR") < 0)
        CCTK_WARN(0, "could not set SCALAR value in table!");
  
    ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, handle_evolved_scalars_bound, 
                      "ScalarWaveKerrSchild::evolved_scalars", "scalar");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Scalar BC for ScalarWaveKerrSchild::evolved_scalars!");
  
  }
  
  if (CCTK_EQUALS(pi_group_bound, "scalar"))
  {
   /* select scalar boundary condition */
    static CCTK_INT handle_pi_group_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_pi_group_bound < 0) handle_pi_group_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_pi_group_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_pi_group_bound ,pi_group_bound_scalar, "SCALAR") < 0)
        CCTK_WARN(0, "could not set SCALAR value in table!");
  
    ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, handle_pi_group_bound, 
                      "ScalarWaveKerrSchild::pi_group", "scalar");
  
    if (ierr < 0)
       CCTK_WARN(0, "Failed to register Scalar BC for ScalarWaveKerrSchild::pi_group!");
  
  }
  
  if (CCTK_EQUALS(phi_bound, "scalar"))
  {
   /* select scalar boundary condition */
    static CCTK_INT handle_phi_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_phi_bound < 0) handle_phi_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_phi_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_phi_bound ,phi_bound_scalar, "SCALAR") < 0)
      CCTK_WARN(0, "could not set SCALAR value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_phi_bound, 
                      "ScalarWaveKerrSchild::phi", "scalar");
  
    if (ierr < 0)
       CCTK_WARN(0, "Error in registering Scalar BC for ScalarWaveKerrSchild::phi!");
  
  }
  
  if (CCTK_EQUALS(rho_bound, "scalar"))
  {
   /* select scalar boundary condition */
    static CCTK_INT handle_rho_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_rho_bound < 0) handle_rho_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_rho_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_rho_bound ,rho_bound_scalar, "SCALAR") < 0)
      CCTK_WARN(0, "could not set SCALAR value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_rho_bound, 
                      "ScalarWaveKerrSchild::rho", "scalar");
  
    if (ierr < 0)
       CCTK_WARN(0, "Error in registering Scalar BC for ScalarWaveKerrSchild::rho!");
  
  }
  
  if (CCTK_EQUALS(pi1_bound, "scalar"))
  {
   /* select scalar boundary condition */
    static CCTK_INT handle_pi1_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_pi1_bound < 0) handle_pi1_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_pi1_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_pi1_bound ,pi1_bound_scalar, "SCALAR") < 0)
      CCTK_WARN(0, "could not set SCALAR value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_pi1_bound, 
                      "ScalarWaveKerrSchild::pi1", "scalar");
  
    if (ierr < 0)
       CCTK_WARN(0, "Error in registering Scalar BC for ScalarWaveKerrSchild::pi1!");
  
  }
  
  if (CCTK_EQUALS(pi2_bound, "scalar"))
  {
   /* select scalar boundary condition */
    static CCTK_INT handle_pi2_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_pi2_bound < 0) handle_pi2_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_pi2_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_pi2_bound ,pi2_bound_scalar, "SCALAR") < 0)
      CCTK_WARN(0, "could not set SCALAR value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_pi2_bound, 
                      "ScalarWaveKerrSchild::pi2", "scalar");
  
    if (ierr < 0)
       CCTK_WARN(0, "Error in registering Scalar BC for ScalarWaveKerrSchild::pi2!");
  
  }
  
  if (CCTK_EQUALS(pi3_bound, "scalar"))
  {
   /* select scalar boundary condition */
    static CCTK_INT handle_pi3_bound CCTK_ATTRIBUTE_UNUSED = -1;
    if (handle_pi3_bound < 0) handle_pi3_bound = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
    if (handle_pi3_bound < 0) CCTK_WARN(0, "could not create table!");
    if (Util_TableSetReal(handle_pi3_bound ,pi3_bound_scalar, "SCALAR") < 0)
      CCTK_WARN(0, "could not set SCALAR value in table!");
  
    ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, handle_pi3_bound, 
                      "ScalarWaveKerrSchild::pi3", "scalar");
  
    if (ierr < 0)
       CCTK_WARN(0, "Error in registering Scalar BC for ScalarWaveKerrSchild::pi3!");
  
  }
  return;
}



/* template for entries in parameter file:
#$bound$#ScalarWaveKerrSchild::evolved_scalars_bound       = "skip"
#$bound$#ScalarWaveKerrSchild::evolved_scalars_bound_speed = 1.0
#$bound$#ScalarWaveKerrSchild::evolved_scalars_bound_limit = 0.0
#$bound$#ScalarWaveKerrSchild::evolved_scalars_bound_scalar = 0.0

#$bound$#ScalarWaveKerrSchild::pi_group_bound       = "skip"
#$bound$#ScalarWaveKerrSchild::pi_group_bound_speed = 1.0
#$bound$#ScalarWaveKerrSchild::pi_group_bound_limit = 0.0
#$bound$#ScalarWaveKerrSchild::pi_group_bound_scalar = 0.0

#$bound$#ScalarWaveKerrSchild::phi_bound       = "skip"
#$bound$#ScalarWaveKerrSchild::phi_bound_speed = 1.0
#$bound$#ScalarWaveKerrSchild::phi_bound_limit = 0.0
#$bound$#ScalarWaveKerrSchild::phi_bound_scalar = 0.0

#$bound$#ScalarWaveKerrSchild::rho_bound       = "skip"
#$bound$#ScalarWaveKerrSchild::rho_bound_speed = 1.0
#$bound$#ScalarWaveKerrSchild::rho_bound_limit = 0.0
#$bound$#ScalarWaveKerrSchild::rho_bound_scalar = 0.0

#$bound$#ScalarWaveKerrSchild::pi1_bound       = "skip"
#$bound$#ScalarWaveKerrSchild::pi1_bound_speed = 1.0
#$bound$#ScalarWaveKerrSchild::pi1_bound_limit = 0.0
#$bound$#ScalarWaveKerrSchild::pi1_bound_scalar = 0.0

#$bound$#ScalarWaveKerrSchild::pi2_bound       = "skip"
#$bound$#ScalarWaveKerrSchild::pi2_bound_speed = 1.0
#$bound$#ScalarWaveKerrSchild::pi2_bound_limit = 0.0
#$bound$#ScalarWaveKerrSchild::pi2_bound_scalar = 0.0

#$bound$#ScalarWaveKerrSchild::pi3_bound       = "skip"
#$bound$#ScalarWaveKerrSchild::pi3_bound_speed = 1.0
#$bound$#ScalarWaveKerrSchild::pi3_bound_limit = 0.0
#$bound$#ScalarWaveKerrSchild::pi3_bound_scalar = 0.0

*/

