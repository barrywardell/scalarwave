/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"
#include "loopcontrol.h"

namespace ScalarWaveKerrSchild {

extern "C" void ScalarWaveKerrSchild_rhs_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (cctk_iteration % ScalarWaveKerrSchild_rhs_calc_every != ScalarWaveKerrSchild_rhs_calc_offset)
    return;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ScalarWaveKerrSchild::evolved_scalarsrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ScalarWaveKerrSchild::evolved_scalarsrhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ScalarWaveKerrSchild::pi_grouprhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ScalarWaveKerrSchild::pi_grouprhs.");
  return;
}

static void ScalarWaveKerrSchild_rhs_Body(const cGH* restrict const cctkGH, const int dir, const int face, const CCTK_REAL normal[3], const CCTK_REAL tangentA[3], const CCTK_REAL tangentB[3], const int imin[3], const int imax[3], const int n_subblock_gfs, CCTK_REAL* restrict const subblock_gfs[])
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = cctk_time;
  const CCTK_REAL cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_TIME;
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 
    0.333333333333333333333333333333;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 
    0.666666666666666666666666666667;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 
    1.33333333333333333333333333333;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5*dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5*dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5*dzi;
  /* Initialize predefined quantities */
  const CCTK_REAL p1o12dx CCTK_ATTRIBUTE_UNUSED = 0.0833333333333333333333333333333*pow(dx,-1);
  const CCTK_REAL p1o12dy CCTK_ATTRIBUTE_UNUSED = 0.0833333333333333333333333333333*pow(dy,-1);
  const CCTK_REAL p1o12dz CCTK_ATTRIBUTE_UNUSED = 0.0833333333333333333333333333333*pow(dz,-1);
  const CCTK_REAL p1o2dx CCTK_ATTRIBUTE_UNUSED = 0.5*pow(dx,-1);
  const CCTK_REAL p1o2dy CCTK_ATTRIBUTE_UNUSED = 0.5*pow(dy,-1);
  const CCTK_REAL p1o2dz CCTK_ATTRIBUTE_UNUSED = 0.5*pow(dz,-1);
  const CCTK_REAL p1o60dx CCTK_ATTRIBUTE_UNUSED = 0.0166666666666666666666666666667*pow(dx,-1);
  const CCTK_REAL p1o60dy CCTK_ATTRIBUTE_UNUSED = 0.0166666666666666666666666666667*pow(dy,-1);
  const CCTK_REAL p1o60dz CCTK_ATTRIBUTE_UNUSED = 0.0166666666666666666666666666667*pow(dz,-1);
  const CCTK_REAL p1o840dx CCTK_ATTRIBUTE_UNUSED = 0.00119047619047619047619047619048*pow(dx,-1);
  const CCTK_REAL p1o840dy CCTK_ATTRIBUTE_UNUSED = 0.00119047619047619047619047619048*pow(dy,-1);
  const CCTK_REAL p1o840dz CCTK_ATTRIBUTE_UNUSED = 0.00119047619047619047619047619048*pow(dz,-1);
  const CCTK_REAL pm1o2dz CCTK_ATTRIBUTE_UNUSED = -0.5*pow(dz,-1);
  /* Jacobian variable pointers */
  const bool usejacobian1 = (!CCTK_IsFunctionAliased("MultiPatch_GetMap") || MultiPatch_GetMap(cctkGH) != jacobian_identity_map)
                        && strlen(jacobian_group) > 0;
  const bool usejacobian = assume_use_jacobian>=0 ? assume_use_jacobian : usejacobian1;
  if (usejacobian && (strlen(jacobian_derivative_group) == 0))
  {
    CCTK_WARN(1, "GenericFD::jacobian_group and GenericFD::jacobian_derivative_group must both be set to valid group names");
  }
  
  const CCTK_REAL* restrict jacobian_ptrs[9];
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_group,
                                                9, jacobian_ptrs);
  
  const CCTK_REAL* restrict const J11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[0] : 0;
  const CCTK_REAL* restrict const J12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[1] : 0;
  const CCTK_REAL* restrict const J13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[2] : 0;
  const CCTK_REAL* restrict const J21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[3] : 0;
  const CCTK_REAL* restrict const J22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[4] : 0;
  const CCTK_REAL* restrict const J23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[5] : 0;
  const CCTK_REAL* restrict const J31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[6] : 0;
  const CCTK_REAL* restrict const J32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[7] : 0;
  const CCTK_REAL* restrict const J33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_determinant_ptrs[1] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_determinant_group) > 0) GroupDataPointers(cctkGH, jacobian_determinant_group,
                                                1, jacobian_determinant_ptrs);
  
  const CCTK_REAL* restrict const detJ CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_determinant_ptrs[0] : 0;
  
  const CCTK_REAL* restrict jacobian_inverse_ptrs[9] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_inverse_group) > 0) GroupDataPointers(cctkGH, jacobian_inverse_group,
                                                9, jacobian_inverse_ptrs);
  
  const CCTK_REAL* restrict const iJ11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[0] : 0;
  const CCTK_REAL* restrict const iJ12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[1] : 0;
  const CCTK_REAL* restrict const iJ13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[2] : 0;
  const CCTK_REAL* restrict const iJ21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[3] : 0;
  const CCTK_REAL* restrict const iJ22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[4] : 0;
  const CCTK_REAL* restrict const iJ23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[5] : 0;
  const CCTK_REAL* restrict const iJ31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[6] : 0;
  const CCTK_REAL* restrict const iJ32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[7] : 0;
  const CCTK_REAL* restrict const iJ33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_derivative_ptrs[18] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_derivative_group,
                                      18, jacobian_derivative_ptrs);
  
  const CCTK_REAL* restrict const dJ111 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[0] : 0;
  const CCTK_REAL* restrict const dJ112 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[1] : 0;
  const CCTK_REAL* restrict const dJ113 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[2] : 0;
  const CCTK_REAL* restrict const dJ122 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[3] : 0;
  const CCTK_REAL* restrict const dJ123 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[4] : 0;
  const CCTK_REAL* restrict const dJ133 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[5] : 0;
  const CCTK_REAL* restrict const dJ211 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[6] : 0;
  const CCTK_REAL* restrict const dJ212 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[7] : 0;
  const CCTK_REAL* restrict const dJ213 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[8] : 0;
  const CCTK_REAL* restrict const dJ222 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[9] : 0;
  const CCTK_REAL* restrict const dJ223 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[10] : 0;
  const CCTK_REAL* restrict const dJ233 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[11] : 0;
  const CCTK_REAL* restrict const dJ311 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[12] : 0;
  const CCTK_REAL* restrict const dJ312 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[13] : 0;
  const CCTK_REAL* restrict const dJ313 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[14] : 0;
  const CCTK_REAL* restrict const dJ322 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[15] : 0;
  const CCTK_REAL* restrict const dJ323 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[16] : 0;
  const CCTK_REAL* restrict const dJ333 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[17] : 0;
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3(ScalarWaveKerrSchild_rhs,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2])
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    /* Assign local copies of grid functions */
    
    CCTK_REAL dphi1L CCTK_ATTRIBUTE_UNUSED = dphi1[index];
    CCTK_REAL dphi2L CCTK_ATTRIBUTE_UNUSED = dphi2[index];
    CCTK_REAL dphi3L CCTK_ATTRIBUTE_UNUSED = dphi3[index];
    CCTK_REAL rhoL CCTK_ATTRIBUTE_UNUSED = rho[index];
    CCTK_REAL xL CCTK_ATTRIBUTE_UNUSED = x[index];
    CCTK_REAL yL CCTK_ATTRIBUTE_UNUSED = y[index];
    CCTK_REAL zL CCTK_ATTRIBUTE_UNUSED = z[index];
    
    
    CCTK_REAL J11L, J12L, J13L, J21L, J22L, J23L, J31L, J32L, J33L CCTK_ATTRIBUTE_UNUSED ;
    
    if (usejacobian)
    {
      J11L = J11[index];
      J12L = J12[index];
      J13L = J13[index];
      J21L = J21[index];
      J22L = J22[index];
      J23L = J23[index];
      J31L = J31[index];
      J32L = J32[index];
      J33L = J33[index];
    }
    /* Include user supplied include files */
    /* Precompute derivatives */
    CCTK_REAL PDstandardNth1dphi1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDstandardNth2dphi1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDstandardNth3dphi1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDstandardNth1dphi2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDstandardNth2dphi2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDstandardNth3dphi2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDstandardNth1dphi3 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDstandardNth2dphi3 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDstandardNth3dphi3 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDstandardNth1rho CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDstandardNth2rho CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDstandardNth3rho CCTK_ATTRIBUTE_UNUSED;
    
    switch (fdOrder)
    {
      case 2:
      {
        PDstandardNth1dphi1 = PDstandardNthfdOrder21(&dphi1[index]);
        PDstandardNth2dphi1 = PDstandardNthfdOrder22(&dphi1[index]);
        PDstandardNth3dphi1 = PDstandardNthfdOrder23(&dphi1[index]);
        PDstandardNth1dphi2 = PDstandardNthfdOrder21(&dphi2[index]);
        PDstandardNth2dphi2 = PDstandardNthfdOrder22(&dphi2[index]);
        PDstandardNth3dphi2 = PDstandardNthfdOrder23(&dphi2[index]);
        PDstandardNth1dphi3 = PDstandardNthfdOrder21(&dphi3[index]);
        PDstandardNth2dphi3 = PDstandardNthfdOrder22(&dphi3[index]);
        PDstandardNth3dphi3 = PDstandardNthfdOrder23(&dphi3[index]);
        PDstandardNth1rho = PDstandardNthfdOrder21(&rho[index]);
        PDstandardNth2rho = PDstandardNthfdOrder22(&rho[index]);
        PDstandardNth3rho = PDstandardNthfdOrder23(&rho[index]);
        break;
      }
      
      case 4:
      {
        PDstandardNth1dphi1 = PDstandardNthfdOrder41(&dphi1[index]);
        PDstandardNth2dphi1 = PDstandardNthfdOrder42(&dphi1[index]);
        PDstandardNth3dphi1 = PDstandardNthfdOrder43(&dphi1[index]);
        PDstandardNth1dphi2 = PDstandardNthfdOrder41(&dphi2[index]);
        PDstandardNth2dphi2 = PDstandardNthfdOrder42(&dphi2[index]);
        PDstandardNth3dphi2 = PDstandardNthfdOrder43(&dphi2[index]);
        PDstandardNth1dphi3 = PDstandardNthfdOrder41(&dphi3[index]);
        PDstandardNth2dphi3 = PDstandardNthfdOrder42(&dphi3[index]);
        PDstandardNth3dphi3 = PDstandardNthfdOrder43(&dphi3[index]);
        PDstandardNth1rho = PDstandardNthfdOrder41(&rho[index]);
        PDstandardNth2rho = PDstandardNthfdOrder42(&rho[index]);
        PDstandardNth3rho = PDstandardNthfdOrder43(&rho[index]);
        break;
      }
      
      case 6:
      {
        PDstandardNth1dphi1 = PDstandardNthfdOrder61(&dphi1[index]);
        PDstandardNth2dphi1 = PDstandardNthfdOrder62(&dphi1[index]);
        PDstandardNth3dphi1 = PDstandardNthfdOrder63(&dphi1[index]);
        PDstandardNth1dphi2 = PDstandardNthfdOrder61(&dphi2[index]);
        PDstandardNth2dphi2 = PDstandardNthfdOrder62(&dphi2[index]);
        PDstandardNth3dphi2 = PDstandardNthfdOrder63(&dphi2[index]);
        PDstandardNth1dphi3 = PDstandardNthfdOrder61(&dphi3[index]);
        PDstandardNth2dphi3 = PDstandardNthfdOrder62(&dphi3[index]);
        PDstandardNth3dphi3 = PDstandardNthfdOrder63(&dphi3[index]);
        PDstandardNth1rho = PDstandardNthfdOrder61(&rho[index]);
        PDstandardNth2rho = PDstandardNthfdOrder62(&rho[index]);
        PDstandardNth3rho = PDstandardNthfdOrder63(&rho[index]);
        break;
      }
      
      case 8:
      {
        PDstandardNth1dphi1 = PDstandardNthfdOrder81(&dphi1[index]);
        PDstandardNth2dphi1 = PDstandardNthfdOrder82(&dphi1[index]);
        PDstandardNth3dphi1 = PDstandardNthfdOrder83(&dphi1[index]);
        PDstandardNth1dphi2 = PDstandardNthfdOrder81(&dphi2[index]);
        PDstandardNth2dphi2 = PDstandardNthfdOrder82(&dphi2[index]);
        PDstandardNth3dphi2 = PDstandardNthfdOrder83(&dphi2[index]);
        PDstandardNth1dphi3 = PDstandardNthfdOrder81(&dphi3[index]);
        PDstandardNth2dphi3 = PDstandardNthfdOrder82(&dphi3[index]);
        PDstandardNth3dphi3 = PDstandardNthfdOrder83(&dphi3[index]);
        PDstandardNth1rho = PDstandardNthfdOrder81(&rho[index]);
        PDstandardNth2rho = PDstandardNthfdOrder82(&rho[index]);
        PDstandardNth3rho = PDstandardNthfdOrder83(&rho[index]);
        break;
      }
      default:
        CCTK_BUILTIN_UNREACHABLE();
    }
    /* Calculate temporaries and grid functions */
    CCTK_REAL JacPDstandardNth1dphi1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL JacPDstandardNth1rho CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL JacPDstandardNth2dphi2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL JacPDstandardNth2rho CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL JacPDstandardNth3dphi3 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL JacPDstandardNth3rho CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      JacPDstandardNth1dphi1 = J11L*PDstandardNth1dphi1 + 
        J21L*PDstandardNth2dphi1 + J31L*PDstandardNth3dphi1;
      
      JacPDstandardNth1rho = J11L*PDstandardNth1rho + J21L*PDstandardNth2rho 
        + J31L*PDstandardNth3rho;
      
      JacPDstandardNth2dphi2 = J12L*PDstandardNth1dphi2 + 
        J22L*PDstandardNth2dphi2 + J32L*PDstandardNth3dphi2;
      
      JacPDstandardNth2rho = J12L*PDstandardNth1rho + J22L*PDstandardNth2rho 
        + J32L*PDstandardNth3rho;
      
      JacPDstandardNth3dphi3 = J13L*PDstandardNth1dphi3 + 
        J23L*PDstandardNth2dphi3 + J33L*PDstandardNth3dphi3;
      
      JacPDstandardNth3rho = J13L*PDstandardNth1rho + J23L*PDstandardNth2rho 
        + J33L*PDstandardNth3rho;
    }
    else
    {
      JacPDstandardNth1dphi1 = PDstandardNth1dphi1;
      
      JacPDstandardNth1rho = PDstandardNth1rho;
      
      JacPDstandardNth2dphi2 = PDstandardNth2dphi2;
      
      JacPDstandardNth2rho = PDstandardNth2rho;
      
      JacPDstandardNth3dphi3 = PDstandardNth3dphi3;
      
      JacPDstandardNth3rho = PDstandardNth3rho;
    }
    
    CCTK_REAL rxyz CCTK_ATTRIBUTE_UNUSED = 
      0.707106781186547524400844362105*pow(pow(xL,2) + pow(yL,2) + pow(zL,2) 
      - pow(a,2) + pow(4*pow(zL,2)*pow(a,2) + pow(pow(xL,2) + pow(yL,2) + 
      pow(zL,2) - pow(a,2),2),0.5),0.5);
    
    CCTK_REAL beta1 CCTK_ATTRIBUTE_UNUSED = 2*M*(yL*a + 
      xL*rxyz)*pow(rxyz,3)*pow(pow(a,2) + 
      pow(rxyz,2),-1)*pow(pow(zL,2)*pow(a,2) + 2*M*pow(rxyz,3) + 
      pow(rxyz,4),-1);
    
    CCTK_REAL beta2 CCTK_ATTRIBUTE_UNUSED = 2*M*(-(xL*a) + 
      yL*rxyz)*pow(rxyz,3)*pow(pow(a,2) + 
      pow(rxyz,2),-1)*pow(pow(zL,2)*pow(a,2) + 2*M*pow(rxyz,3) + 
      pow(rxyz,4),-1);
    
    CCTK_REAL beta3 CCTK_ATTRIBUTE_UNUSED = 
      2*zL*M*pow(rxyz,2)*pow(pow(zL,2)*pow(a,2) + 2*M*pow(rxyz,3) + 
      pow(rxyz,4),-1);
    
    CCTK_REAL alpha CCTK_ATTRIBUTE_UNUSED = pow(1 + 
      2*M*pow(rxyz,3)*pow(pow(zL,2)*pow(a,2) + pow(rxyz,4),-1),-0.5);
    
    CCTK_REAL gamma CCTK_ATTRIBUTE_UNUSED = (2*M*rxyz*pow(zL,2)*pow(a,2) + 
      pow(zL,2)*pow(a,4) + pow(zL,2)*pow(a,2)*pow(rxyz,2) + 2*M*(pow(xL,2) + 
      pow(yL,2) + pow(zL,2))*pow(rxyz,3) + pow(a,2)*pow(rxyz,4) + 
      pow(rxyz,6))*pow(pow(a,2) + pow(rxyz,2),-1)*pow(pow(zL,2)*pow(a,2) + 
      pow(rxyz,4),-1);
    
    CCTK_REAL phirhsL CCTK_ATTRIBUTE_UNUSED = rhoL;
    
    CCTK_REAL pi1rhsL CCTK_ATTRIBUTE_UNUSED = JacPDstandardNth1rho;
    
    CCTK_REAL pi2rhsL CCTK_ATTRIBUTE_UNUSED = JacPDstandardNth2rho;
    
    CCTK_REAL pi3rhsL CCTK_ATTRIBUTE_UNUSED = JacPDstandardNth3rho;
    
    CCTK_REAL rhorhsL CCTK_ATTRIBUTE_UNUSED = beta1*JacPDstandardNth1rho + 
      beta2*JacPDstandardNth2rho + beta3*JacPDstandardNth3rho + 
      alpha*(JacPDstandardNth1dphi1 + JacPDstandardNth2dphi2 + 
      JacPDstandardNth3dphi3)*pow(gamma,-0.5);
    /* Copy local copies back to grid functions */
    phirhs[index] = phirhsL;
    pi1rhs[index] = pi1rhsL;
    pi2rhs[index] = pi2rhsL;
    pi3rhs[index] = pi3rhsL;
    rhorhs[index] = rhorhsL;
  }
  CCTK_ENDLOOP3(ScalarWaveKerrSchild_rhs);
}
extern "C" void ScalarWaveKerrSchild_rhs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering ScalarWaveKerrSchild_rhs_Body");
  }
  if (cctk_iteration % ScalarWaveKerrSchild_rhs_calc_every != ScalarWaveKerrSchild_rhs_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "ScalarWaveKerrSchild::dphi_group",
    "ScalarWaveKerrSchild::evolved_scalars",
    "ScalarWaveKerrSchild::evolved_scalarsrhs",
    "grid::coordinates",
    "ScalarWaveKerrSchild::pi_grouprhs"};
  AssertGroupStorage(cctkGH, "ScalarWaveKerrSchild_rhs", 5, groups);
  
  switch (fdOrder)
  {
    case 2:
    {
      EnsureStencilFits(cctkGH, "ScalarWaveKerrSchild_rhs", 1, 1, 1);
      break;
    }
    
    case 4:
    {
      EnsureStencilFits(cctkGH, "ScalarWaveKerrSchild_rhs", 2, 2, 2);
      break;
    }
    
    case 6:
    {
      EnsureStencilFits(cctkGH, "ScalarWaveKerrSchild_rhs", 3, 3, 3);
      break;
    }
    
    case 8:
    {
      EnsureStencilFits(cctkGH, "ScalarWaveKerrSchild_rhs", 4, 4, 4);
      break;
    }
    default:
      CCTK_BUILTIN_UNREACHABLE();
  }
  
  LoopOverInterior(cctkGH, ScalarWaveKerrSchild_rhs_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving ScalarWaveKerrSchild_rhs_Body");
  }
}

} // namespace ScalarWaveKerrSchild
