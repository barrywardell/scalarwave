/*  File produced by Kranc */

#include "cctk.h"

extern "C" int DGWaveADMBase_Startup(void)
{
  const char* banner CCTK_ATTRIBUTE_UNUSED = "DGWaveADMBase";
  CCTK_RegisterBanner(banner);
  return 0;
}
