/*  File produced by Kranc */

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

extern "C" void DGWaveADMBase_RegisterVars(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  /* Register all the evolved grid functions with MoL */
  ierr += MoLRegisterEvolved(CCTK_VarIndex("DGWaveADMBase::Psi"),  CCTK_VarIndex("DGWaveADMBase::Psirhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("DGWaveADMBase::dJ0"),  CCTK_VarIndex("DGWaveADMBase::dJ0rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("DGWaveADMBase::J1"),  CCTK_VarIndex("DGWaveADMBase::J1rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("DGWaveADMBase::J2"),  CCTK_VarIndex("DGWaveADMBase::J2rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("DGWaveADMBase::J3"),  CCTK_VarIndex("DGWaveADMBase::J3rhs"));
  /* Register all the evolved Array functions with MoL */
  return;
}
