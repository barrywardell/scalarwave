/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"
#include "loopcontrol.h"
#include "hrscc.hh"

namespace DGWaveADMBase {


/* DGFE Definitions */

#define config_sdg_order      4
#define config_riemann_solver hrscc::LaxFriedrichsRS<DGFE_DGWaveADMBase_GreenFunctionInitialData, false>

/* Export definitions */
#define DGWaveADMBase_GreenFunctionInitialData_sdg_grid   hrscc::GNIGrid<hrscc::GLLElement<config_sdg_order> >
#define DGWaveADMBase_GreenFunctionInitialData_sdg_method hrscc::SDGMethod<DGFE_DGWaveADMBase_GreenFunctionInitialData, DGWaveADMBase_GreenFunctionInitialData_sdg_grid, config_riemann_solver>

/*** Numerical scheme ***/

/* Configuration */
#define config_method DGWaveADMBase_GreenFunctionInitialData_sdg_method

/* Export definitions */
#define DGWaveADMBase_GreenFunctionInitialData_method config_method
#define DGWaveADMBase_GreenFunctionInitialData_solver hrscc::CLawSolver<DGFE_DGWaveADMBase_GreenFunctionInitialData, config_method>



class DGFE_DGWaveADMBase_GreenFunctionInitialData;

} // namespace CCTK_THORN
namespace hrscc {
  using namespace CCTK_THORN;
  template<>
  struct traits<DGFE_DGWaveADMBase_GreenFunctionInitialData> {
    // All state vector variables
    enum state_t {nvars};
    enum {nequations = nvars};
    enum {nexternal = 3*nvars};
    enum {nbitmasks = 0};
    static const bool pure = false;
  };
} // namespace hrscc
namespace CCTK_THORN {



class DGFE_DGWaveADMBase_GreenFunctionInitialData: public hrscc::CLaw<DGFE_DGWaveADMBase_GreenFunctionInitialData> {
public:
  typedef hrscc::CLaw<DGFE_DGWaveADMBase_GreenFunctionInitialData> claw;
  typedef hrscc::traits<DGFE_DGWaveADMBase_GreenFunctionInitialData>::state_t state_t;
  typedef hrscc::traits<DGFE_DGWaveADMBase_GreenFunctionInitialData> variables_t;
  static const int nvars = variables_t::nvars;
  
  DGFE_DGWaveADMBase_GreenFunctionInitialData();
  
  inline void prim_to_all(hrscc::Observer<claw> & observer) const
  {
  }
  
  template<hrscc::policy::direction_t dir>
  inline void fluxes(hrscc::Observer<claw> & observer) const
  {
    
    
    switch (dir) {
    case hrscc::policy::x: {
      break;
    }
    case hrscc::policy::y: {
      break;
    }
    case hrscc::policy::z: {
      break;
    }
    default:
      CCTK_BUILTIN_UNREACHABLE();
    }
    
  }
  
  template<hrscc::policy::direction_t dir>
  inline void eigenvalues(hrscc::Observer<claw> & observer) const
  {
    assert(0);
  }
  
  template<hrscc::policy::direction_t dir>
  inline void eig(hrscc::Observer<claw> & observer) const
  {
    assert(0);
  }
};



} // namespace CCTK_THORN
namespace hrscc {
  using namespace CCTK_THORN;
  template<> int CLaw<DGFE_DGWaveADMBase_GreenFunctionInitialData>::conserved_idx[DGFE_DGWaveADMBase_GreenFunctionInitialData::nvars] = {};
  template<> int CLaw<DGFE_DGWaveADMBase_GreenFunctionInitialData>::primitive_idx[DGFE_DGWaveADMBase_GreenFunctionInitialData::nvars] = {};
  template<> int CLaw<DGFE_DGWaveADMBase_GreenFunctionInitialData>::rhs_idx[DGFE_DGWaveADMBase_GreenFunctionInitialData::nvars] = {};
  template<> int CLaw<DGFE_DGWaveADMBase_GreenFunctionInitialData>::field_idx[3*DGFE_DGWaveADMBase_GreenFunctionInitialData::nvars] = {};
  template<> int CLaw<DGFE_DGWaveADMBase_GreenFunctionInitialData>::bitmask_idx[0] = {};
} // namespace hrscc
namespace CCTK_THORN {



namespace {
  int varindex(const char* const varname)
  {
    const int vi = CCTK_VarIndex(varname);
    if (vi<0) CCTK_ERROR("Internal error");
    return vi;
  }
}

DGFE_DGWaveADMBase_GreenFunctionInitialData::DGFE_DGWaveADMBase_GreenFunctionInitialData()
{
  using namespace hrscc;



}



/* A solver, DGFE's equivalent of cctkGH */
static DGWaveADMBase_GreenFunctionInitialData_solver *solver = NULL;



/* Call the pointwise DGFE derivative operator */
#undef PDstandardNth1
#undef PDstandardNth2
#undef PDstandardNth3
#define PDstandardNth1(u) (solver->wdiff<hrscc::policy::x>(&(u)[-index], i,j,k))
#define PDstandardNth2(u) (solver->wdiff<hrscc::policy::y>(&(u)[-index], i,j,k))
#define PDstandardNth3(u) (solver->wdiff<hrscc::policy::z>(&(u)[-index], i,j,k))



static void DGWaveADMBase_GreenFunctionInitialData_Body(const cGH* restrict const cctkGH, const int dir, const int face, const CCTK_REAL normal[3], const CCTK_REAL tangentA[3], const CCTK_REAL tangentB[3], const int imin[3], const int imax[3], const int n_subblock_gfs, CCTK_REAL* restrict const subblock_gfs[])
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = cctk_time;
  const CCTK_REAL cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_TIME;
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 
    0.333333333333333333333333333333;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 
    0.666666666666666666666666666667;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 
    1.33333333333333333333333333333;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5*dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5*dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5*dzi;
  /* Initialize predefined quantities */
  const CCTK_REAL p1o2dx CCTK_ATTRIBUTE_UNUSED = 0.5*pow(dx,-1);
  const CCTK_REAL p1o2dy CCTK_ATTRIBUTE_UNUSED = 0.5*pow(dy,-1);
  const CCTK_REAL p1o2dz CCTK_ATTRIBUTE_UNUSED = 0.5*pow(dz,-1);
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3(DGWaveADMBase_GreenFunctionInitialData,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2])
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    /* Assign local copies of grid functions */
    
    CCTK_REAL alpL CCTK_ATTRIBUTE_UNUSED = alp[index];
    CCTK_REAL gxxL CCTK_ATTRIBUTE_UNUSED = gxx[index];
    CCTK_REAL gxyL CCTK_ATTRIBUTE_UNUSED = gxy[index];
    CCTK_REAL gxzL CCTK_ATTRIBUTE_UNUSED = gxz[index];
    CCTK_REAL gyyL CCTK_ATTRIBUTE_UNUSED = gyy[index];
    CCTK_REAL gyzL CCTK_ATTRIBUTE_UNUSED = gyz[index];
    CCTK_REAL gzzL CCTK_ATTRIBUTE_UNUSED = gzz[index];
    CCTK_REAL xL CCTK_ATTRIBUTE_UNUSED = x[index];
    CCTK_REAL yL CCTK_ATTRIBUTE_UNUSED = y[index];
    CCTK_REAL zL CCTK_ATTRIBUTE_UNUSED = z[index];
    
    /* Include user supplied include files */
    /* Precompute derivatives */
    /* Calculate temporaries and grid functions */
    CCTK_REAL gamma CCTK_ATTRIBUTE_UNUSED = 2*gxyL*gxzL*gyzL - 
      gzzL*pow(gxyL,2) + gyyL*(gxxL*gzzL - pow(gxzL,2)) - gxxL*pow(gyzL,2);
    
    CCTK_REAL PsiL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL dJ0L CCTK_ATTRIBUTE_UNUSED = -(amplitude*exp(-0.5*(pow(-6 + 
      xL,2) + pow(yL,2) + 
      pow(zL,2))*pow(width,-2))*pow(alpL,-1)*pow(gamma,0.5));
    
    CCTK_REAL J1L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL J2L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL J3L CCTK_ATTRIBUTE_UNUSED = 0;
    /* Copy local copies back to grid functions */
    dJ0[index] = dJ0L;
    J1[index] = J1L;
    J2[index] = J2L;
    J3[index] = J3L;
    Psi[index] = PsiL;
  }
  CCTK_ENDLOOP3(DGWaveADMBase_GreenFunctionInitialData);
}
extern "C" void DGWaveADMBase_GreenFunctionInitialData(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering DGWaveADMBase_GreenFunctionInitialData_Body");
  }
  if (cctk_iteration % DGWaveADMBase_GreenFunctionInitialData_calc_every != DGWaveADMBase_GreenFunctionInitialData_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "admbase::lapse",
    "admbase::metric",
    "DGWaveADMBase::evolved_scalars",
    "grid::coordinates",
    "DGWaveADMBase::J_group"};
  AssertGroupStorage(cctkGH, "DGWaveADMBase_GreenFunctionInitialData", 5, groups);
  
  
  
  if (not solver) solver = new DGWaveADMBase_GreenFunctionInitialData_method(cctkGH);
  LoopOverEverything(cctkGH, DGWaveADMBase_GreenFunctionInitialData_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving DGWaveADMBase_GreenFunctionInitialData_Body");
  }
}

} // namespace DGWaveADMBase
