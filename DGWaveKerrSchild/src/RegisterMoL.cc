/*  File produced by Kranc */

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

extern "C" void DGWaveKerrSchild_RegisterVars(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  /* Register all the evolved grid functions with MoL */
  ierr += MoLRegisterEvolved(CCTK_VarIndex("DGWaveKerrSchild::Psi"),  CCTK_VarIndex("DGWaveKerrSchild::Psirhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("DGWaveKerrSchild::J0"),  CCTK_VarIndex("DGWaveKerrSchild::J0rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("DGWaveKerrSchild::J1"),  CCTK_VarIndex("DGWaveKerrSchild::J1rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("DGWaveKerrSchild::J2"),  CCTK_VarIndex("DGWaveKerrSchild::J2rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("DGWaveKerrSchild::J3"),  CCTK_VarIndex("DGWaveKerrSchild::J3rhs"));
  /* Register all the evolved Array functions with MoL */
  return;
}
