/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"
#include "loopcontrol.h"
#include "hrscc.hh"

namespace DGWaveKerrSchild {


/* DGFE Definitions */

#define config_sdg_order      4
#define config_riemann_solver hrscc::LaxFriedrichsRS<DGFE_DGWaveKerrSchild_rhs, false>

/* Export definitions */
#define DGWaveKerrSchild_rhs_sdg_grid   hrscc::GNIGrid<hrscc::GLLElement<config_sdg_order> >
#define DGWaveKerrSchild_rhs_sdg_method hrscc::SDGMethod<DGFE_DGWaveKerrSchild_rhs, DGWaveKerrSchild_rhs_sdg_grid, config_riemann_solver>

/*** Numerical scheme ***/

/* Configuration */
#define config_method DGWaveKerrSchild_rhs_sdg_method

/* Export definitions */
#define DGWaveKerrSchild_rhs_method config_method
#define DGWaveKerrSchild_rhs_solver hrscc::CLawSolver<DGFE_DGWaveKerrSchild_rhs, config_method>



class DGFE_DGWaveKerrSchild_rhs;

} // namespace CCTK_THORN
namespace hrscc {
  using namespace CCTK_THORN;
  template<>
  struct traits<DGFE_DGWaveKerrSchild_rhs> {
    // All state vector variables
    enum state_t {iJ0, iJ1, iJ2, iJ3, iPsi, nvars};
    enum {nequations = nvars};
    enum {nexternal = 3*nvars};
    enum {nbitmasks = 0};
    static const bool pure = false;
  };
} // namespace hrscc
namespace CCTK_THORN {



class DGFE_DGWaveKerrSchild_rhs: public hrscc::CLaw<DGFE_DGWaveKerrSchild_rhs> {
public:
  typedef hrscc::CLaw<DGFE_DGWaveKerrSchild_rhs> claw;
  typedef hrscc::traits<DGFE_DGWaveKerrSchild_rhs>::state_t state_t;
  typedef hrscc::traits<DGFE_DGWaveKerrSchild_rhs> variables_t;
  static const int nvars = variables_t::nvars;
  
  DGFE_DGWaveKerrSchild_rhs();
  
  inline void prim_to_all(hrscc::Observer<claw> & observer) const
  {
  }
  
  template<hrscc::policy::direction_t dir>
  inline void fluxes(hrscc::Observer<claw> & observer) const
  {
    
    CCTK_REAL fluxJ0L;
    CCTK_REAL fluxJ1L;
    CCTK_REAL fluxJ2L;
    CCTK_REAL fluxJ3L;
    CCTK_REAL fluxPsiL;
    
    switch (dir) {
    case hrscc::policy::x: {
      fluxJ0L = observer.field[variables_t::iJ0 + 0*DGFE_DGWaveKerrSchild_rhs::nvars];
      fluxJ1L = observer.field[variables_t::iJ1 + 0*DGFE_DGWaveKerrSchild_rhs::nvars];
      fluxJ2L = observer.field[variables_t::iJ2 + 0*DGFE_DGWaveKerrSchild_rhs::nvars];
      fluxJ3L = observer.field[variables_t::iJ3 + 0*DGFE_DGWaveKerrSchild_rhs::nvars];
      fluxPsiL = observer.field[variables_t::iPsi + 0*DGFE_DGWaveKerrSchild_rhs::nvars];
      break;
    }
    case hrscc::policy::y: {
      fluxJ0L = observer.field[variables_t::iJ0 + 1*DGFE_DGWaveKerrSchild_rhs::nvars];
      fluxJ1L = observer.field[variables_t::iJ1 + 1*DGFE_DGWaveKerrSchild_rhs::nvars];
      fluxJ2L = observer.field[variables_t::iJ2 + 1*DGFE_DGWaveKerrSchild_rhs::nvars];
      fluxJ3L = observer.field[variables_t::iJ3 + 1*DGFE_DGWaveKerrSchild_rhs::nvars];
      fluxPsiL = observer.field[variables_t::iPsi + 1*DGFE_DGWaveKerrSchild_rhs::nvars];
      break;
    }
    case hrscc::policy::z: {
      fluxJ0L = observer.field[variables_t::iJ0 + 2*DGFE_DGWaveKerrSchild_rhs::nvars];
      fluxJ1L = observer.field[variables_t::iJ1 + 2*DGFE_DGWaveKerrSchild_rhs::nvars];
      fluxJ2L = observer.field[variables_t::iJ2 + 2*DGFE_DGWaveKerrSchild_rhs::nvars];
      fluxJ3L = observer.field[variables_t::iJ3 + 2*DGFE_DGWaveKerrSchild_rhs::nvars];
      fluxPsiL = observer.field[variables_t::iPsi + 2*DGFE_DGWaveKerrSchild_rhs::nvars];
      break;
    }
    default:
      CCTK_BUILTIN_UNREACHABLE();
    }
    
    observer.flux[dir][variables_t::iJ0] = fluxJ0L;
    observer.flux[dir][variables_t::iJ1] = fluxJ1L;
    observer.flux[dir][variables_t::iJ2] = fluxJ2L;
    observer.flux[dir][variables_t::iJ3] = fluxJ3L;
    observer.flux[dir][variables_t::iPsi] = fluxPsiL;
  }
  
  template<hrscc::policy::direction_t dir>
  inline void eigenvalues(hrscc::Observer<claw> & observer) const
  {
    assert(0);
  }
  
  template<hrscc::policy::direction_t dir>
  inline void eig(hrscc::Observer<claw> & observer) const
  {
    assert(0);
  }
};



} // namespace CCTK_THORN
namespace hrscc {
  using namespace CCTK_THORN;
  template<> int CLaw<DGFE_DGWaveKerrSchild_rhs>::conserved_idx[DGFE_DGWaveKerrSchild_rhs::nvars] = {};
  template<> int CLaw<DGFE_DGWaveKerrSchild_rhs>::primitive_idx[DGFE_DGWaveKerrSchild_rhs::nvars] = {};
  template<> int CLaw<DGFE_DGWaveKerrSchild_rhs>::rhs_idx[DGFE_DGWaveKerrSchild_rhs::nvars] = {};
  template<> int CLaw<DGFE_DGWaveKerrSchild_rhs>::field_idx[3*DGFE_DGWaveKerrSchild_rhs::nvars] = {};
  template<> int CLaw<DGFE_DGWaveKerrSchild_rhs>::bitmask_idx[0] = {};
} // namespace hrscc
namespace CCTK_THORN {



namespace {
  int varindex(const char* const varname)
  {
    const int vi = CCTK_VarIndex(varname);
    if (vi<0) CCTK_ERROR("Internal error");
    return vi;
  }
}

DGFE_DGWaveKerrSchild_rhs::DGFE_DGWaveKerrSchild_rhs()
{
  using namespace hrscc;

  CLaw<DGFE_DGWaveKerrSchild_rhs>::conserved_idx[variables_t::iJ0] = varindex(CCTK_THORNSTRING "::J0");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::conserved_idx[variables_t::iJ1] = varindex(CCTK_THORNSTRING "::J1");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::conserved_idx[variables_t::iJ2] = varindex(CCTK_THORNSTRING "::J2");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::conserved_idx[variables_t::iJ3] = varindex(CCTK_THORNSTRING "::J3");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::conserved_idx[variables_t::iPsi] = varindex(CCTK_THORNSTRING "::Psi");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::primitive_idx[variables_t::iJ0] = varindex(CCTK_THORNSTRING "::J0");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::primitive_idx[variables_t::iJ1] = varindex(CCTK_THORNSTRING "::J1");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::primitive_idx[variables_t::iJ2] = varindex(CCTK_THORNSTRING "::J2");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::primitive_idx[variables_t::iJ3] = varindex(CCTK_THORNSTRING "::J3");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::primitive_idx[variables_t::iPsi] = varindex(CCTK_THORNSTRING "::Psi");

  CLaw<DGFE_DGWaveKerrSchild_rhs>::field_idx[variables_t::iJ0 + 0*DGFE_DGWaveKerrSchild_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxJ01");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::field_idx[variables_t::iJ1 + 0*DGFE_DGWaveKerrSchild_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxJ11");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::field_idx[variables_t::iJ2 + 0*DGFE_DGWaveKerrSchild_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxJ21");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::field_idx[variables_t::iJ3 + 0*DGFE_DGWaveKerrSchild_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxJ31");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::field_idx[variables_t::iPsi + 0*DGFE_DGWaveKerrSchild_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxPsi1");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::field_idx[variables_t::iJ0 + 1*DGFE_DGWaveKerrSchild_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxJ02");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::field_idx[variables_t::iJ1 + 1*DGFE_DGWaveKerrSchild_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxJ12");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::field_idx[variables_t::iJ2 + 1*DGFE_DGWaveKerrSchild_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxJ22");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::field_idx[variables_t::iJ3 + 1*DGFE_DGWaveKerrSchild_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxJ32");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::field_idx[variables_t::iPsi + 1*DGFE_DGWaveKerrSchild_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxPsi2");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::field_idx[variables_t::iJ0 + 2*DGFE_DGWaveKerrSchild_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxJ03");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::field_idx[variables_t::iJ1 + 2*DGFE_DGWaveKerrSchild_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxJ13");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::field_idx[variables_t::iJ2 + 2*DGFE_DGWaveKerrSchild_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxJ23");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::field_idx[variables_t::iJ3 + 2*DGFE_DGWaveKerrSchild_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxJ33");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::field_idx[variables_t::iPsi + 2*DGFE_DGWaveKerrSchild_rhs::nvars] = varindex(CCTK_THORNSTRING "::fluxPsi3");

  CLaw<DGFE_DGWaveKerrSchild_rhs>::rhs_idx[variables_t::iJ0] = varindex(CCTK_THORNSTRING "::J0rhs");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::rhs_idx[variables_t::iJ1] = varindex(CCTK_THORNSTRING "::J1rhs");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::rhs_idx[variables_t::iJ2] = varindex(CCTK_THORNSTRING "::J2rhs");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::rhs_idx[variables_t::iJ3] = varindex(CCTK_THORNSTRING "::J3rhs");
  CLaw<DGFE_DGWaveKerrSchild_rhs>::rhs_idx[variables_t::iPsi] = varindex(CCTK_THORNSTRING "::Psirhs");
}



/* A solver, DGFE's equivalent of cctkGH */
static DGWaveKerrSchild_rhs_solver *solver = NULL;



/* Call the pointwise DGFE derivative operator */
#undef PDstandardNth1
#undef PDstandardNth2
#undef PDstandardNth3
#define PDstandardNth1(u) (solver->wdiff<hrscc::policy::x>(&(u)[-index], i,j,k))
#define PDstandardNth2(u) (solver->wdiff<hrscc::policy::y>(&(u)[-index], i,j,k))
#define PDstandardNth3(u) (solver->wdiff<hrscc::policy::z>(&(u)[-index], i,j,k))



static void DGWaveKerrSchild_rhs_Body(const cGH* restrict const cctkGH, const int dir, const int face, const CCTK_REAL normal[3], const CCTK_REAL tangentA[3], const CCTK_REAL tangentB[3], const int imin[3], const int imax[3], const int n_subblock_gfs, CCTK_REAL* restrict const subblock_gfs[])
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = cctk_time;
  const CCTK_REAL cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_TIME;
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 
    0.333333333333333333333333333333;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 
    0.666666666666666666666666666667;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 
    1.33333333333333333333333333333;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5*dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5*dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5*dzi;
  /* Initialize predefined quantities */
  const CCTK_REAL p1o2dx CCTK_ATTRIBUTE_UNUSED = 0.5*pow(dx,-1);
  const CCTK_REAL p1o2dy CCTK_ATTRIBUTE_UNUSED = 0.5*pow(dy,-1);
  const CCTK_REAL p1o2dz CCTK_ATTRIBUTE_UNUSED = 0.5*pow(dz,-1);
  /* Jacobian variable pointers */
  const bool usejacobian1 = (!CCTK_IsFunctionAliased("MultiPatch_GetMap") || MultiPatch_GetMap(cctkGH) != jacobian_identity_map)
                        && strlen(jacobian_group) > 0;
  const bool usejacobian = assume_use_jacobian>=0 ? assume_use_jacobian : usejacobian1;
  if (usejacobian && (strlen(jacobian_derivative_group) == 0))
  {
    CCTK_WARN(1, "GenericFD::jacobian_group and GenericFD::jacobian_derivative_group must both be set to valid group names");
  }
  
  const CCTK_REAL* restrict jacobian_ptrs[9];
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_group,
                                                9, jacobian_ptrs);
  
  const CCTK_REAL* restrict const J11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[0] : 0;
  const CCTK_REAL* restrict const J12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[1] : 0;
  const CCTK_REAL* restrict const J13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[2] : 0;
  const CCTK_REAL* restrict const J21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[3] : 0;
  const CCTK_REAL* restrict const J22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[4] : 0;
  const CCTK_REAL* restrict const J23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[5] : 0;
  const CCTK_REAL* restrict const J31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[6] : 0;
  const CCTK_REAL* restrict const J32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[7] : 0;
  const CCTK_REAL* restrict const J33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_determinant_ptrs[1] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_determinant_group) > 0) GroupDataPointers(cctkGH, jacobian_determinant_group,
                                                1, jacobian_determinant_ptrs);
  
  const CCTK_REAL* restrict const detJ CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_determinant_ptrs[0] : 0;
  
  const CCTK_REAL* restrict jacobian_inverse_ptrs[9] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_inverse_group) > 0) GroupDataPointers(cctkGH, jacobian_inverse_group,
                                                9, jacobian_inverse_ptrs);
  
  const CCTK_REAL* restrict const iJ11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[0] : 0;
  const CCTK_REAL* restrict const iJ12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[1] : 0;
  const CCTK_REAL* restrict const iJ13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[2] : 0;
  const CCTK_REAL* restrict const iJ21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[3] : 0;
  const CCTK_REAL* restrict const iJ22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[4] : 0;
  const CCTK_REAL* restrict const iJ23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[5] : 0;
  const CCTK_REAL* restrict const iJ31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[6] : 0;
  const CCTK_REAL* restrict const iJ32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[7] : 0;
  const CCTK_REAL* restrict const iJ33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_derivative_ptrs[18] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_derivative_group,
                                      18, jacobian_derivative_ptrs);
  
  const CCTK_REAL* restrict const dJ111 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[0] : 0;
  const CCTK_REAL* restrict const dJ112 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[1] : 0;
  const CCTK_REAL* restrict const dJ113 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[2] : 0;
  const CCTK_REAL* restrict const dJ122 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[3] : 0;
  const CCTK_REAL* restrict const dJ123 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[4] : 0;
  const CCTK_REAL* restrict const dJ133 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[5] : 0;
  const CCTK_REAL* restrict const dJ211 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[6] : 0;
  const CCTK_REAL* restrict const dJ212 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[7] : 0;
  const CCTK_REAL* restrict const dJ213 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[8] : 0;
  const CCTK_REAL* restrict const dJ222 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[9] : 0;
  const CCTK_REAL* restrict const dJ223 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[10] : 0;
  const CCTK_REAL* restrict const dJ233 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[11] : 0;
  const CCTK_REAL* restrict const dJ311 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[12] : 0;
  const CCTK_REAL* restrict const dJ312 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[13] : 0;
  const CCTK_REAL* restrict const dJ313 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[14] : 0;
  const CCTK_REAL* restrict const dJ322 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[15] : 0;
  const CCTK_REAL* restrict const dJ323 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[16] : 0;
  const CCTK_REAL* restrict const dJ333 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[17] : 0;
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3(DGWaveKerrSchild_rhs,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2])
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    /* Assign local copies of grid functions */
    
    CCTK_REAL J0L CCTK_ATTRIBUTE_UNUSED = J0[index];
    CCTK_REAL J1L CCTK_ATTRIBUTE_UNUSED = J1[index];
    CCTK_REAL J2L CCTK_ATTRIBUTE_UNUSED = J2[index];
    CCTK_REAL J3L CCTK_ATTRIBUTE_UNUSED = J3[index];
    CCTK_REAL xL CCTK_ATTRIBUTE_UNUSED = x[index];
    CCTK_REAL yL CCTK_ATTRIBUTE_UNUSED = y[index];
    CCTK_REAL zL CCTK_ATTRIBUTE_UNUSED = z[index];
    
    
    /* Include user supplied include files */
    /* Precompute derivatives */
    /* Calculate temporaries and grid functions */
    CCTK_REAL rxyz CCTK_ATTRIBUTE_UNUSED = 
      0.707106781186547524400844362105*pow(pow(xL,2) + pow(yL,2) + pow(zL,2) 
      - pow(a,2) + pow(4*pow(zL,2)*pow(a,2) + pow(pow(xL,2) + pow(yL,2) + 
      pow(zL,2) - pow(a,2),2),0.5),0.5);
    
    CCTK_REAL beta1 CCTK_ATTRIBUTE_UNUSED = 2*M*(yL*a + 
      xL*rxyz)*pow(rxyz,3)*pow(pow(a,2) + 
      pow(rxyz,2),-1)*pow(pow(zL,2)*pow(a,2) + 2*M*pow(rxyz,3) + 
      pow(rxyz,4),-1);
    
    CCTK_REAL beta2 CCTK_ATTRIBUTE_UNUSED = 2*M*(-(xL*a) + 
      yL*rxyz)*pow(rxyz,3)*pow(pow(a,2) + 
      pow(rxyz,2),-1)*pow(pow(zL,2)*pow(a,2) + 2*M*pow(rxyz,3) + 
      pow(rxyz,4),-1);
    
    CCTK_REAL beta3 CCTK_ATTRIBUTE_UNUSED = 
      2*zL*M*pow(rxyz,2)*pow(pow(zL,2)*pow(a,2) + 2*M*pow(rxyz,3) + 
      pow(rxyz,4),-1);
    
    CCTK_REAL alpha CCTK_ATTRIBUTE_UNUSED = pow(1 + 
      2*M*pow(rxyz,3)*pow(pow(zL,2)*pow(a,2) + pow(rxyz,4),-1),-0.5);
    
    CCTK_REAL gamma CCTK_ATTRIBUTE_UNUSED = (2*M*rxyz*pow(zL,2)*pow(a,2) + 
      pow(zL,2)*pow(a,4) + pow(zL,2)*pow(a,2)*pow(rxyz,2) + 2*M*(pow(xL,2) + 
      pow(yL,2) + pow(zL,2))*pow(rxyz,3) + pow(a,2)*pow(rxyz,4) + 
      pow(rxyz,6))*pow(pow(a,2) + pow(rxyz,2),-1)*pow(pow(zL,2)*pow(a,2) + 
      pow(rxyz,4),-1);
    
    CCTK_REAL gammauu11 CCTK_ATTRIBUTE_UNUSED = 1 - 
      pow(alpha,-2)*pow(beta1,2) - 2*M*pow(rxyz,3)*pow(yL*a + 
      xL*rxyz,2)*pow(pow(a,2) + pow(rxyz,2),-2)*pow(pow(zL,2)*pow(a,2) + 
      pow(rxyz,4),-1);
    
    CCTK_REAL gammauu21 CCTK_ATTRIBUTE_UNUSED = 
      -(beta1*beta2*pow(alpha,-2)) - 2*M*(yL*a + xL*rxyz)*(-(xL*a) + 
      yL*rxyz)*pow(rxyz,3)*pow(pow(a,2) + 
      pow(rxyz,2),-2)*pow(pow(zL,2)*pow(a,2) + pow(rxyz,4),-1);
    
    CCTK_REAL gammauu31 CCTK_ATTRIBUTE_UNUSED = 
      -(beta1*beta3*pow(alpha,-2)) - 2*zL*M*(yL*a + 
      xL*rxyz)*pow(rxyz,2)*pow(pow(a,2) + 
      pow(rxyz,2),-1)*pow(pow(zL,2)*pow(a,2) + pow(rxyz,4),-1);
    
    CCTK_REAL gammauu22 CCTK_ATTRIBUTE_UNUSED = 1 - 
      pow(alpha,-2)*pow(beta2,2) - 2*M*pow(rxyz,3)*pow(-(xL*a) + 
      yL*rxyz,2)*pow(pow(a,2) + pow(rxyz,2),-2)*pow(pow(zL,2)*pow(a,2) + 
      pow(rxyz,4),-1);
    
    CCTK_REAL gammauu32 CCTK_ATTRIBUTE_UNUSED = 
      -(beta2*beta3*pow(alpha,-2)) - 2*zL*M*(-(xL*a) + 
      yL*rxyz)*pow(rxyz,2)*pow(pow(a,2) + 
      pow(rxyz,2),-1)*pow(pow(zL,2)*pow(a,2) + pow(rxyz,4),-1);
    
    CCTK_REAL gammauu33 CCTK_ATTRIBUTE_UNUSED = 1 - 
      pow(alpha,-2)*pow(beta3,2) - 2*M*rxyz*pow(zL,2)*pow(pow(zL,2)*pow(a,2) 
      + pow(rxyz,4),-1);
    
    CCTK_REAL dtPsi CCTK_ATTRIBUTE_UNUSED = J1L*beta1 + J2L*beta2 + 
      J3L*beta3 - J0L*alpha*pow(gamma,-0.5);
    
    CCTK_REAL fluxJ11L CCTK_ATTRIBUTE_UNUSED = -dtPsi;
    
    CCTK_REAL fluxJ12L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL fluxJ13L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL fluxJ21L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL fluxJ22L CCTK_ATTRIBUTE_UNUSED = -dtPsi;
    
    CCTK_REAL fluxJ23L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL fluxJ31L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL fluxJ32L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL fluxJ33L CCTK_ATTRIBUTE_UNUSED = -dtPsi;
    
    CCTK_REAL fluxJ01L CCTK_ATTRIBUTE_UNUSED = -(J0L*beta1) + 
      alpha*(J1L*gammauu11 + J2L*gammauu21 + J3L*gammauu31)*pow(gamma,0.5);
    
    CCTK_REAL fluxJ02L CCTK_ATTRIBUTE_UNUSED = -(J0L*beta2) + 
      alpha*(J1L*gammauu21 + J2L*gammauu22 + J3L*gammauu32)*pow(gamma,0.5);
    
    CCTK_REAL fluxJ03L CCTK_ATTRIBUTE_UNUSED = -(J0L*beta3) + 
      alpha*(J1L*gammauu31 + J2L*gammauu32 + J3L*gammauu33)*pow(gamma,0.5);
    
    CCTK_REAL fluxPsi1L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL fluxPsi2L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL fluxPsi3L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL J0rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL J1rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL J2rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL J3rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL PsirhsL CCTK_ATTRIBUTE_UNUSED = dtPsi;
    /* Copy local copies back to grid functions */
    fluxJ01[index] = fluxJ01L;
    fluxJ02[index] = fluxJ02L;
    fluxJ03[index] = fluxJ03L;
    fluxJ11[index] = fluxJ11L;
    fluxJ12[index] = fluxJ12L;
    fluxJ13[index] = fluxJ13L;
    fluxJ21[index] = fluxJ21L;
    fluxJ22[index] = fluxJ22L;
    fluxJ23[index] = fluxJ23L;
    fluxJ31[index] = fluxJ31L;
    fluxJ32[index] = fluxJ32L;
    fluxJ33[index] = fluxJ33L;
    fluxPsi1[index] = fluxPsi1L;
    fluxPsi2[index] = fluxPsi2L;
    fluxPsi3[index] = fluxPsi3L;
    J0rhs[index] = J0rhsL;
    J1rhs[index] = J1rhsL;
    J2rhs[index] = J2rhsL;
    J3rhs[index] = J3rhsL;
    Psirhs[index] = PsirhsL;
  }
  CCTK_ENDLOOP3(DGWaveKerrSchild_rhs);
}
extern "C" void DGWaveKerrSchild_rhs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering DGWaveKerrSchild_rhs_Body");
  }
  if (cctk_iteration % DGWaveKerrSchild_rhs_calc_every != DGWaveKerrSchild_rhs_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "DGWaveKerrSchild::evolved_scalars",
    "DGWaveKerrSchild::evolved_scalarsrhs",
    "DGWaveKerrSchild::fluxJ0_group",
    "DGWaveKerrSchild::fluxJ_group",
    "DGWaveKerrSchild::fluxPsi_group",
    "grid::coordinates",
    "DGWaveKerrSchild::J_group",
    "DGWaveKerrSchild::J_grouprhs"};
  AssertGroupStorage(cctkGH, "DGWaveKerrSchild_rhs", 8, groups);
  
  
  
  if (not solver) solver = new DGWaveKerrSchild_rhs_method(cctkGH);
  LoopOverEverything(cctkGH, DGWaveKerrSchild_rhs_Body);
  
  /* Add the flux terms to the RHS */
  solver->compute_rhs();
  
  delete solver;
  solver = NULL;
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving DGWaveKerrSchild_rhs_Body");
  }
}

} // namespace DGWaveKerrSchild
