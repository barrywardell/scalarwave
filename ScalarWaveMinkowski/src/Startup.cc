/*  File produced by Kranc */

#include "cctk.h"

extern "C" int ScalarWaveMinkowski_Startup(void)
{
  const char* banner CCTK_ATTRIBUTE_UNUSED = "ScalarWaveMinkowski";
  CCTK_RegisterBanner(banner);
  return 0;
}
