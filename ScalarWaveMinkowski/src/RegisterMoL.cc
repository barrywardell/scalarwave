/*  File produced by Kranc */

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

extern "C" void ScalarWaveMinkowski_RegisterVars(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  /* Register all the evolved grid functions with MoL */
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ScalarWaveMinkowski::phi"),  CCTK_VarIndex("ScalarWaveMinkowski::phirhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ScalarWaveMinkowski::rho"),  CCTK_VarIndex("ScalarWaveMinkowski::rhorhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ScalarWaveMinkowski::pi1"),  CCTK_VarIndex("ScalarWaveMinkowski::pi1rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ScalarWaveMinkowski::pi2"),  CCTK_VarIndex("ScalarWaveMinkowski::pi2rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ScalarWaveMinkowski::pi3"),  CCTK_VarIndex("ScalarWaveMinkowski::pi3rhs"));
  /* Register all the evolved Array functions with MoL */
  return;
}
